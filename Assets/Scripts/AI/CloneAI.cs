﻿using UnityEngine;
using System.Collections;

public class CloneAI : AI 
{
    Rigidbody rb;
    Vector2 targetDirection;
    GameObject target;
    VesselInput input;
    public string targetTag;

    Vector2 weaponOffset;
    Vector2 movementOffset;
    Vector2 oldWeaponDirection;

    Hub cloneHub;

    public float weaponJitter = 0;
    public float movementJitter = 2f;

    VesselInput player;
    PlayerInput playerInput;

    void Start()
    {
        movementOffset = Dice.Vector2(movementJitter);
        weaponOffset = Dice.Vector2(weaponJitter);
        rb = GetComponent<Rigidbody>();
        input = GetComponent<VesselInput>();
        cloneHub = transform.parent.GetComponent<Hub>();

        playerInput = GameObject.FindGameObjectWithTag(gameObject.GetTeamName()).GetComponent<PlayerInput>();
    }

    void Update()
    {
        player = playerInput.Vessel;
        cloneHub = transform.parent.GetComponent<Hub>();
        //UpdateMovementAxis();
        if (input != player)
        {
            UpdateWeaponAxis();
            UpdateTriggerAxis();
        }
    }

    void UpdateMovementAxis()
    {
        input.movementAxis = Vector2.Lerp(input.movementAxis, targetDirection + MovementOffset, 0.1f);
        input.movementAxis = input.movementAxis.normalized;
    }

    void UpdateWeaponAxis()
    {
        targetDirection = GetWeaponTargetDirection();
        input.weaponAxis = targetDirection;
    }

    void UpdateTriggerAxis()
    {
        if (target != null)
        {
            if (gameObject.GetDistTo2D(target) < 200 &&  Dice.Chance(50)) input.triggerAxis = 1;
            else input.triggerAxis = 0;
        }
        else
        {
            input.triggerAxis = 0;
        }
    }


    Vector2 GetWeaponTargetDirection()
    {
        GetWeaponTarget();
        Vector2 direction = new Vector2();

        if (target != null)
        {
            
            direction = gameObject.GetVector2To(target);
            direction = Vector2.Lerp(oldWeaponDirection, direction.normalized + WeaponOffset, 0.1f);
            direction = direction.normalized;
            //Debug.Log(direction);
            oldWeaponDirection = direction;
        }

        return direction;
    }



    GameObject GetWeaponTarget()
    {

        target = cloneHub.SelectEnemyClone(gameObject);
        return target;
    }

    Vector2 MovementOffset
    {
        get
        {
            if (Dice.Chance(20))
            {
                return movementOffset = Dice.Vector2(movementJitter);
            }
            else return movementOffset;
        }
    }

    Vector2 WeaponOffset
    {
        get
        {
            if (Dice.Chance(20))
            {
                return weaponOffset = Dice.Vector2(weaponJitter);
            }
            else return weaponOffset;
        }
    }
}
