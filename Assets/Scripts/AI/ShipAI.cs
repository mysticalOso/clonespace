﻿using UnityEngine;
using System.Collections;


//TODO sepearate out AIStates
public class ShipAI : AI 
{

    Rigidbody rb;
    Vector2 targetDirection;
    GameObject target;
    VesselInput input;

    public Vector2 weaponOffset;
    public Vector2 movementOffset;

    Vector2 targetWeaponOffset;
    Vector2 targetMovementOffset;

    public float weaponJitter = 0.1f;
    public float movementJitter = 0.1f;

    PlayerInput playerInput;

    VesselInput player;
    VesselInput oldPlayer;
    
    Hub shipHub;

    int followerSlot;

    void Start()
    {
        playerInput = GameObject.FindGameObjectWithTag(gameObject.GetTeamName()).GetComponent<PlayerInput>();
        Debug.Log(playerInput);
        targetMovementOffset = Dice.Vector2(movementJitter);
        targetWeaponOffset = Dice.Vector2(weaponJitter);
        rb = GetComponent<Rigidbody>();
        input = GetComponent<VesselInput>();
        shipHub = GameObject.Find("Ships").GetComponent<Hub>();
    }

    void Update()
    {
        UpdatePlayer();
        input.TogglePlayerUI(IsPlayer);
        if (!IsPlayer)
        {
            UpdateMovementAxis();
            UpdateWeaponAxis();
            UpdateTriggerAxis();
        }

    }

    void UpdatePlayer()
    {

        player = playerInput.Vessel;
        if (player != oldPlayer)
        {
            if (oldPlayer!=null) oldPlayer.UnFollow();
            if (!IsPlayer) followerSlot = player.Follow();
        }
        oldPlayer = player;
    }

    void UpdateMovementAxis()
    {
        if (FollowTargetDistance > 30)
        {
            input.movementAxis = FollowDirection;
            input.movementAxis = input.movementAxis.normalized;
        }
        else
        {
            input.movementAxis = new Vector2();
        }
    }

    void UpdateWeaponAxis()
    {
        targetDirection = GetWeaponTargetDirection();
        if (target != null) AdjustDirectionForSpeed();
        input.weaponAxis = targetDirection;
    }

    void UpdateTriggerAxis()
    {
        if (target != null)
        {
            //if (gameObject.GetDistTo2D(target) < 200 && Dice.Chance(20)) 
            if (gameObject.GetDistTo2D(target) < 200) input.triggerAxis = 1;
            else input.triggerAxis = 0;
        }
        else
        {
            input.triggerAxis = 0;
        }
    }
    Vector2 FollowDirection
    {
        get { return gameObject.GetDirectionTo(FollowTarget); }
    }

    Vector2 FollowTarget
    {
        get { return player.transform.position + player.Rotation * FollowOffset; }
    }

    Vector2 FollowOffset
    {
        get
        {
            if (followerSlot == 1) return new Vector2(50, -50);
            else return new Vector2(-50, -50);
        }
    }

    float FollowTargetDistance
    {
        get
        {
            return Vector2.Distance(transform.position, FollowTarget);
            
        }
    }

    Vector2 GetMovementDirection()
    {
        GetWeaponTarget();
        Vector2 direction = new Vector2();

        if (target != null)
        {
            direction = gameObject.GetDirectionTo((Vector2)target.transform.position + MovementOffset);
        }

        return direction;
    }

    Vector2 GetWeaponTargetDirection()
    {
        GetWeaponTarget();
        Vector2 direction = new Vector2();

        if (target != null)
        {
            direction = gameObject.GetNextVector2To(target);
            direction = direction.normalized + WeaponOffset;
            direction = direction.normalized;
        }

        return direction;
    }

    void AdjustDirectionForSpeed()
    {
        Vector2 relativeVelocity = gameObject.GetVelocityRelativeTo2D(target);
        float relativeSpeed = relativeVelocity.magnitude;

        float lazerSpeed = 400f;

        float angleFromVelocityToTarget = relativeVelocity.AngleTo(targetDirection);

        float angleOffset = Mathf.Asin(( relativeSpeed * Mathf.Sin( angleFromVelocityToTarget )) / lazerSpeed);
        if (targetDirection.magnitude > 0)
        {
            targetDirection = targetDirection.RotateVector(-angleOffset);
        }

        
    }

    GameObject GetWeaponTarget()
    {
        target = shipHub.GetClosestEnemyShip(gameObject,gameObject.tag);
        return target;
    }

    Vector2 MovementOffset
    {
        get
        {
            if (Dice.Chance(10))
            {
                targetMovementOffset = Dice.Vector2(movementJitter);
            }
            movementOffset = Vector2.Lerp(movementOffset, targetMovementOffset, 0.05f);
            return movementOffset;
        }
    }

    Vector2 WeaponOffset
    {
        get
        {
            if (Dice.Chance(10))
            {
                targetWeaponOffset = Dice.Vector2(weaponJitter);
            }
            weaponOffset = Vector2.Lerp(weaponOffset, targetWeaponOffset, 0.05f);
            return weaponOffset;
        }
    }

    public bool IsPlayer
    {
        get { return (input == player); }
    }

}
