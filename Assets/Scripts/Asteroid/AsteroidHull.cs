﻿using UnityEngine;
using System.Collections;

public class AsteroidHull : Hull 
{

    public override void CreateExplosion()
    {
        GameObject e = Instantiate(explosion);
        e.transform.position = transform.position;
        e.transform.rotation = transform.rotation;
        e.transform.localScale = transform.localScale;
        e.GetComponent<InsideMeshExplosion>().MeshObject = gameObject;
        GetComponent<MeshExploder>().Explode();
    }


}
