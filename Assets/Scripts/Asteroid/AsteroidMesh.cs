﻿using UnityEngine;
using System.Collections.Generic;

public class AsteroidMesh : MonoBehaviour 
{
    AsteroidMeshGen meshGen;

	void Start () 
	{
        meshGen = new AsteroidMeshGen();
        GetComponent<MeshFilter>().mesh = meshGen.Generate();

        List<Vector2> outline = meshGen.Outline;

        GetComponent<PolygonCollider2D>().SetPath(0, outline.ToArray());

        gameObject.GetComponent<MeshExploder>().enabled = true;
    }

	void Update () 
	{
	
	}

}
