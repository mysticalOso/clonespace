﻿using UnityEngine;
using System.Collections.Generic;

public class AsteroidMeshGen
{
    BlobTextureGen blobGen;
    OutlineGen outlineGen;
    TriMeshGen triMeshGen;
    MeshModifier meshModifier;
    Mesh mesh;

    public AsteroidMeshGen()
    {

    }

    public Mesh Generate()
    {
        bool tryAgain = false;
        do
        {
            try
            {
                if (tryAgain) Debug.Log("Actually Retrying Asteroid mesh generation");
                blobGen = new BlobTextureGen();
                outlineGen = new OutlineGen();
                triMeshGen = new TriMeshGen();
                meshModifier = new MeshModifier();
                GenBlobTexture();
                GenOutline();
                GenTriMesh();
                RoundMesh();
                AddNoiseToMesh();
                FlatShade();
                OptimiseMesh();
                tryAgain = false;
            }
            catch
            {
                tryAgain = true;
                Debug.Log("Retrying Asteroid mesh generation");
            }
        } while (tryAgain);
        return mesh;
        
    }

    void GenBlobTexture()
    {
        blobGen.GenBlob(50, 50, 50);
        blobGen.ApplyNoise(0.01f, 0.5f);
        blobGen.ApplyNoise(0.02f, 0.5f);
    }

    void GenOutline()
    {
        outlineGen.GenOutline(blobGen.Values, 100, 100);
    }

    void GenTriMesh()
    {
        mesh = triMeshGen.GenerateMesh(outlineGen.Poly, 10);
    }

    void RoundMesh()
    {
        meshModifier.RoundMesh(mesh, outlineGen.Poly);
    }

    void AddNoiseToMesh()
    {
        meshModifier.AddNoise(mesh, 1, 0.1f);
        meshModifier.AddNoise(mesh, 4, 0.04f);
    }

    void FlatShade()
    {
        meshModifier.FlatShade(mesh);
    }

    void OptimiseMesh()
    {
        meshModifier.Optimise(mesh);
    }

    public List<Vector2> Outline
    {
        get { return outlineGen.Poly; }
    }

}
