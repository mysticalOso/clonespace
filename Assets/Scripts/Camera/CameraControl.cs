﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour 
{

    float movementTransition = 1;
    float rotationTransition = 1;
    float zoomTransition = 1;
    
    GameObject oldMovementTarget;
    float startZoom;
    Quaternion startRotation;
    GameObject movementTarget = null;
    GameObject rotationTarget = null;
    VesselInput input;
    float zoomTarget = -500;
    Vector2 basePosition;
    Vector2 oldOffset = new Vector2();

    float transTime = 0;
    float totalTime = 2f;

    public Rect targetRect = new Rect();
    public Rect startRect;


    private Camera camera1;

    void Start () 
	{
        camera1 = GetComponent<Camera>();
        startRect = camera1.rect;
        GetComponent<Camera>().rect = targetRect;
    }

	void LateUpdate () 
	{

            if (movementTransition < 1) UpdateMovementTransition();
            else UpdateMovement();


            UpdateOffsetPosition();

            if (rotationTransition < 1) UpdateRotationTransition();
            else UpdateRotation();


            if (zoomTransition < 1) UpdateZoomTransition();
            /*
        if (transTime < 2f)
        {
            transTime+= Time.deltaTime;
            float ratio = (float)transTime /2f;
            Rect newRect = new Rect();
            newRect.x = Mathf.Lerp(startRect.x, targetRect.x, ratio);
            newRect.y = Mathf.Lerp(startRect.y, targetRect.y, ratio);
            newRect.xMax = Mathf.Lerp(startRect.xMax, targetRect.xMax, ratio);
            newRect.yMax = Mathf.Lerp(startRect.yMax, targetRect.yMax, ratio);
            camera1.rect = newRect;
        }
        else if (transTime > 2f)
        {
            transTime = 2f;
            float ratio = (float)transTime / 2f;
            Rect newRect = new Rect();
            newRect.x = Mathf.Lerp(startRect.x, targetRect.x, ratio);
            newRect.y = Mathf.Lerp(startRect.y, targetRect.y, ratio);
            newRect.xMax = Mathf.Lerp(startRect.xMax, targetRect.xMax, ratio);
            newRect.yMax = Mathf.Lerp(startRect.yMax, targetRect.yMax, ratio);
            camera1.rect = newRect;
        }*/

        
	}

    void UpdateOffsetPosition()
    {
        Vector2 offset = Vector2.Lerp(oldOffset, transform.rotation * input.weaponAxis * (transform.position.z / -20f), 0.05f);
        oldOffset = offset;
        Vector2 pos = basePosition + offset;
        transform.position = new Vector3(pos.x, pos.y, transform.position.z);
    }

    void UpdateMovementTransition()
    {
        movementTransition += Time.deltaTime;
        float targetX = movementTarget.transform.position.x;
        float targetY = movementTarget.transform.position.y;
        float newX = Mathf.SmoothStep(StartMovement.x, targetX, movementTransition);
        float newY = Mathf.SmoothStep(StartMovement.y, targetY, movementTransition);
        basePosition = new Vector3(newX, newY, transform.position.z);
    }

    void UpdateMovement()
    {
        if (movementTarget != null)
        {
            float targetX = movementTarget.transform.position.x;
            float targetY = movementTarget.transform.position.y;
            basePosition = new Vector3(targetX, targetY, transform.position.z);
        }

    }

    void UpdateRotationTransition()
    {
        rotationTransition += Time.deltaTime;
        float ratio = Mathf.SmoothStep(0, 1, rotationTransition);
        transform.rotation = Quaternion.Slerp(startRotation, TargetRotation, ratio);
    }

    void UpdateRotation()
    {
        transform.rotation = TargetRotation;
    }

    void UpdateZoomTransition()
    {
        zoomTransition += Time.deltaTime;
        float newZ = Mathf.SmoothStep(startZoom, zoomTarget, zoomTransition);
        transform.position = new Vector3(transform.position.x, transform.position.y, newZ);
    }



    public void ZoomFar(GameObject ship)
    {
        ZoomTarget = -700;
        RotationTarget = null;
        MovementTarget = ship;
    }

    public void ZoomMid(GameObject ship)
    {
        ZoomTarget = -500;
        RotationTarget = null;
        MovementTarget = ship;
    }

    public void ZoomClose(GameObject ship, GameObject clone)
    {
        ZoomTarget = -15;
        RotationTarget = ship;
        MovementTarget = clone;
    }

    public void ZoomCloseFar(GameObject ship, GameObject clone)
    {
        ZoomTarget = -20;
        RotationTarget = ship;
        MovementTarget = clone;
    }

    public GameObject RotationTarget
    {
        set
        {
            if (rotationTarget != value)
            {
                rotationTarget = value;

                rotationTransition = 0;
                startRotation = transform.rotation;
            }
        }
    }

    public Quaternion TargetRotation
    {
        get
        {
            if (rotationTarget == null)
            {
                return Quaternion.LookRotation(Vector3.forward, Vector2.up);
            }
            else
            {
                return rotationTarget.transform.rotation;
            }
        }
    }

    public GameObject MovementTarget
    {
        set
        {
            if (movementTarget != value)
            {
                
                oldMovementTarget = movementTarget;
                movementTarget = value;
                movementTransition = 0;
                if (oldMovementTarget == null) oldMovementTarget = movementTarget;
                if (movementTarget != null) input = movementTarget.GetComponent<VesselInput>();
            }
        }
    }

    Vector2 StartMovement
    {
        get
        {
            return oldMovementTarget.transform.position;
        }
    }


    public float ZoomTarget
    {
        set
        {
            if (zoomTarget != value)
            {
                zoomTarget = value;
                zoomTransition = 0;
                startZoom = transform.position.z;
            }
        }
    }

    bool IsCloneTarget()
    {
        return rotationTarget != null;
    }

    public Vector2 TargetOffset
    {
        get
        {
            //Debug.Log(camera1.WorldToScreenPoint(target.transform.position));
            return camera1.WorldToScreenPoint(movementTarget.transform.position);
        }
    }

}
