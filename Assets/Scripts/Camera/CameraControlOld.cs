﻿using UnityEngine;
using System.Collections;

public class CameraControlOld : MonoBehaviour {
    
    public GameObject target;
    private Camera camera1;
    public float targetZ;
    public VesselInput input;
    Rigidbody rb;
    bool lockRotation = false;
    Vector2 targetDirection;

    public float powerLimit = 50;
    public float powerMultiple = 1f;

    // Use this for initialization
    void Start () 
    {
        rb = GetComponent<Rigidbody>();
        camera1 = GetComponent<Camera>();
        input = target.GetComponent<VesselInput>();

	}

    public Vector2 TargetOffset
    {
        get
        {
            //Debug.Log(camera1.WorldToScreenPoint(target.transform.position));
            return camera1.WorldToScreenPoint(target.transform.position);
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        
        Vector3 targetFuturePos = target.transform.position + (Vector3)target.GetVelocity() * 0.5f;
        Vector3 diff = targetFuturePos - transform.position + (Vector3)input.weaponAxis * 20f;
        diff.z = targetZ - transform.position.z;
        rb.AddForce(diff*30);

        UpdateRotation();
	}

    void UpdateRotation()
    {
        if (lockRotation) targetDirection = target.transform.up;
        else targetDirection = Vector2.up;

        Vector2 currentDirection = new Vector2(transform.up.x, transform.up.y);


        float angleTo = Vector2.Angle(currentDirection, targetDirection);

        Vector3 cross = Vector3.Cross(currentDirection, targetDirection);

        if (cross.z > 0)
            angleTo = -angleTo;

        float torque = -angleTo * powerMultiple;


        torque = Mathf.Clamp(torque, -powerLimit, powerLimit);

        rb.AddTorque(new Vector3(0,0,torque));
        

    }

    public void ZoomFar()
    {
        targetZ = -1000;
        lockRotation = false;
    }

    public void ZoomMid()
    {
        targetZ = -500;
        lockRotation = false;
    }

    public void ZoomClose()
    {
        targetZ = -20;
        lockRotation = true;
    }

    public GameObject Target
    {
        set
        {
            target = value;
        }
    }
}
