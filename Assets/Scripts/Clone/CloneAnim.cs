﻿using UnityEngine;
using System.Collections;

public class CloneAnim : MonoBehaviour 
{
    VesselInput input;
    Animator anim;

    void Start () 
	{
        input = GetComponent<VesselInput>();
        anim = GetComponentInChildren<Animator>();
    }

	void Update () 
	{

            anim.SetBool("isShooting", input.IsFiring);

	}

}
