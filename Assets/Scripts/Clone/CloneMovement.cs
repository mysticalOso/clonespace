﻿using UnityEngine;
using System.Collections;

public class CloneMovement : MonoBehaviour 
{
    VesselInput input;
    DummyRigidbody dummy;

    public float speed;
    public float acceleration;

    void Start()
    {
        input = GetComponent<VesselInput>();
        dummy = GetComponent<DummyRigidbody>();
        speed *= transform.localScale.x;
    }

	void FixedUpdate() 
	{
        Vector2 targetVelocity = input.movementAxis * speed;
        dummy.Velocity = Vector2.Lerp(dummy.Velocity, targetVelocity, acceleration);
        dummy.CenterOfMass = new Vector2();

    }

}
