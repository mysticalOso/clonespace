﻿using UnityEngine;
using System.Collections;

public class CloneTurning : MonoBehaviour
{
    VesselInput input;
    DummyRigidbody dummy;

    public float powerLimit = 50;
    public float powerMultiple = 1f;
    Vector2 targetDirection;

    VesselInput player;
    PlayerInput playerInput;

    void Start()
    {
        dummy = GetComponent<DummyRigidbody>();
        input = GetComponent<VesselInput>();
        targetDirection = Vector2.up;
        playerInput = GameObject.FindGameObjectWithTag(gameObject.GetTeamName()).GetComponent<PlayerInput>();
    }

    void FixedUpdate()
    {
        Vector2 currentDirection = new Vector2(transform.up.x, transform.up.y);
        player = playerInput.Vessel;

        if (input.WeaponMagnitude != 0)
        {
            if (input == player)
            {
                targetDirection = transform.parent.rotation * input.WeaponDirection;
            }
            else
            {
                targetDirection = input.WeaponDirection;
            }
        }
        else
        {
            targetDirection = currentDirection;
        }


        float angleTo = Vector2.Angle(currentDirection, targetDirection);

        Vector3 cross = Vector3.Cross(currentDirection, targetDirection);

        if (cross.z > 0)
            angleTo = -angleTo;

        float torque = -angleTo * powerMultiple;


        torque = Mathf.Clamp(torque, -powerLimit, powerLimit);

        dummy.AddTorque(torque);
        
        

    }
}
