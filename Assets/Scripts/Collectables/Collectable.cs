﻿using UnityEngine;
using System.Collections;

public class Collectable : MonoBehaviour {

    public float xRange;
    public float yRange;
    ScoreControl score;
    Rigidbody rb;

	void Start () 
    {
        score = GameObject.FindGameObjectWithTag("Score").GetComponent<ScoreControl>();
        rb = GetComponent<Rigidbody>();
        Move();
        float speed = 50;
        rb.velocity = new Vector3(Random.Range(-speed, speed), Random.Range(-speed, speed), 0);
	}
	
	void OnTriggerEnter(Collider other)
    {
        
        Move();
    }

    void Move()
    {
        Vector3 newPosition = new Vector3();
        newPosition.x = Random.Range(-xRange, xRange);
        newPosition.y = Random.Range(-yRange, yRange);
        transform.position = newPosition;
        float speed = 50;
        rb.velocity = new Vector3(Random.Range(-speed, speed), Random.Range(-speed, speed), 0);
    }
}
