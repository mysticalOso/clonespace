﻿using UnityEngine;
using System.Collections;

public static class Dice
{
    public static Vector2 Vector2(float fromX, float fromY, float toX, float toY)
    {
        Vector2 v = new Vector2();
        v.x = Random.Range(fromX, toX);
        v.y = Random.Range(fromY, toY);
        return v;
    }

    public static Vector2 Vector2(float halfSquare)
    {
        return Vector2(-halfSquare, halfSquare);
    }

    public static Vector2 Vector2(float from, float to)
    {
        return Vector2(new Vector2(from, from), new Vector2(to, to));
    }
    public static Vector2 Vector2(Vector2 from, Vector2 to)
    {
        return Vector2(from.x, from.y, to.x, to.y);
    }

    public static bool Chance(int oneIn)
    {
        int roll = Random.Range(0, oneIn);
        return (roll == 0);
    }

}
