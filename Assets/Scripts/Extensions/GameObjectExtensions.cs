﻿using UnityEngine;
using System.Collections;

public static class GameObjectExtensions
{

    public static Vector2 GetNextVector2To(this GameObject subject, GameObject target)
    {
        Rigidbody2D rb = subject.GetComponent<Rigidbody2D>();
        Rigidbody2D targetRb = target.GetComponent<Rigidbody2D>();
        Vector2 targetVector = target.transform.position + (Vector3)targetRb.velocity * 0.02f;
        Vector2 subjectVector = subject.transform.position + (Vector3)rb.velocity * 0.02f;
        return targetVector - subjectVector;
    }

    public static Vector2 GetVector2To(this GameObject subject, GameObject target)
    {
        return target.transform.position - subject.transform.position;
    }

    public static Vector2 GetDirectionTo(this GameObject subject, GameObject target)
    {
        return GetVector2To(subject, target).normalized;
    }

    public static Vector2 GetDirectionTo(this GameObject subject, Vector2 pos)
    {
        Vector2 direction = pos - (Vector2)subject.transform.position; 
        return direction.normalized;
    }

    public static Vector3 GetVectorTo(this GameObject subject, GameObject target)
    {
        return target.transform.position - subject.transform.position;
    }

    public static GameObject FindClosest2D(this GameObject subject, GameObject[] targets)
    {

        float minDist = Mathf.Infinity;
        float dist = 0;
        GameObject result = null;
        Vector2 diff;

        foreach (GameObject target in targets)
        {
            diff = target.transform.position - subject.transform.position;
            dist = diff.sqrMagnitude;
            if (dist < minDist)
            {
                result = target;
                minDist = dist;
            }
        }

        return result;
       
    }
    public static Vector2 GetVelocity(this GameObject subject)
    {
        Rigidbody2D rb = subject.GetComponent<Rigidbody2D>();
        return rb.velocity;
    }

    public static Vector2 GetVelocityRelativeTo2D(this GameObject subject, GameObject target)
    {
        Rigidbody2D rb = subject.GetComponent<Rigidbody2D>();
        Rigidbody2D targetRb = target.GetComponent<Rigidbody2D>();
        Vector2 targetVelocity = targetRb.velocity;
        Vector2 subjectVelocity = rb.velocity;
        Vector2 relativeVelocity = subjectVelocity - targetVelocity;
        return relativeVelocity;
    }

    public static float GetSqrDistTo2D(this GameObject subject, GameObject target)
    {
        return subject.GetVector2To(target).sqrMagnitude;
    }

    public static float GetDistTo2D(this GameObject subject, GameObject target)
    {
        return subject.GetVector2To(target).magnitude;
    }

    public static GameObject GetParent(this GameObject subject)
    {
        if (subject.transform.parent != null)
        {
            return subject.transform.parent.gameObject;
        }
        else
        {
            return null;
        }
    }


    //CLONE SPACE SPECIFIC

    public static bool IsShip(this GameObject subject)
    {
        return (subject.tag.Substring(2) == "Ship");
    }

    public static bool IsClone(this GameObject subject)
    {
        return (subject.tag.Substring(2) == "Clone");
    }

    public static bool IsAsteroid(this GameObject subject)
    {
        return subject.tag == "Asteroid";
    }

    public static bool IsShield(this GameObject subject)
    {
        return (subject.tag.Substring(2) == "Shield");
    }

    public static bool IsBullet(this GameObject subject)
    {
        return (subject.tag == "Missile" || subject.tag == "Lazer");
    }

    public static bool IsEnemyOf(this GameObject subject, GameObject target)
    {
        return !subject.IsFriendOf(target);
    }

    public static bool IsEnemyOf(this GameObject subject, string teamname)
    {
        return !subject.IsFriendOf(teamname);
    }

    public static bool IsFriendOf(this GameObject subject, GameObject target)
    {
        return (subject.tag.Substring(0, 2) == target.tag.Substring(0, 2));
    }

    public static bool IsFriendOf(this GameObject subject, string teamname)
    {
        return (subject.tag.Substring(0, 2) == teamname);
    }

    public static string GetTeamName(this GameObject subject)
    {
        return subject.tag.Substring(0, 2);
    }






}
