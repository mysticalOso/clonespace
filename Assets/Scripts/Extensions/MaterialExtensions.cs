﻿using UnityEngine;
using System.Collections;

public static class MaterialExtensions
{

    public static void SetAlpha(this Material material, float value)
    {
        Color color = material.color;
        color.a = value;
        material.color = color;
    }

}
