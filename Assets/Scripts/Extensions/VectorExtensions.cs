﻿using UnityEngine;
using System.Collections.Generic;

public static class VectorExtensions
{

    public static Vector2 RotateVector(this Vector2 vector, float radians)
    {
        vector = Quaternion.Euler(0, 0, radians * Mathf.Rad2Deg) * vector;
        return vector;
    }

    public static float AngleTo(this Vector2 start, Vector2 finish)
    {
        float angleTo = Mathf.Deg2Rad * Vector2.Angle(start, finish);
        Vector3 cross = Vector3.Cross(start, finish);

        if (cross.z > 0)
            angleTo = -angleTo;

        return angleTo;
    }

    public static float DistToPoly(this Vector2 point, List<Vector2> poly)
    {
        Vector2 p1, p2, point1;
        point1 = point;
        float dist, minDist;
        int i, j;
        minDist = float.MaxValue;

        for (i = 0; i < poly.Count; i++)
        {
            j = (i + 1) % poly.Count;


            p1 = poly[i];
            p2 = poly[j];

            float r = Vector2.Dot(p2 - p1, point1 - p1);

            float l1 = (point1 - p1).magnitude;

            if (l1 > 0)
            {
                r /= (p1 - p2).magnitude * (p1 - p2).magnitude;

                if (r < 0) dist = l1;
                else if (r > 1) dist = (p2 - point1).magnitude;
                else
                {
                    Vector2 p = new Vector2(p1.x + r * (p2.x - p1.x), p1.y + r * (p2.y - p1.y));
                    dist = (point1 - p).magnitude;
                }
            }
            else
            {
                dist = 0;
            }
            minDist = Mathf.Min(dist, minDist);
        }

        return minDist;
    }

}
