﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;
//TODO use states to spilt ShipControl vs SelectionControl
public class PlayerInput : MonoBehaviour 
{
    public string playerString;
    public VesselInput vessel;
    public VesselInput ship;
    public VesselInput clone;
    public CursorControl cursor;
    public Hub hub;
    public Hub shipHub;
    public CameraControl cam;
    public bool oldButtonA = false;
    public bool oldButtonX = false;
    public Vector2 selectionAxis;
    GamePadState gamepad;

    enum Mode { Ship, Clone, SelectClone, SelectShip, Teleport };
    Mode mode = Mode.Ship;

	void Start () 
	{
        shipHub = GameObject.Find("Ships").GetComponent<Hub>();
        if (ship == null)
        {
            ship = shipHub.GetFirst(tag).GetComponent<VesselInput>();
        }
        cam.ZoomMid(ship.gameObject);
        shipHub = hub;
        GameObject c = ship.CloneHub.GetFirst(tag);
        if (c!= null)
        {
            clone = c.GetComponent<VesselInput>();
        }
        else
        {
            c = null;
        }
        
        vessel = ship;
        //if (vessel != null) vessel.ToggleAI(false);

    }

    void Update()
    {
        GetGamePadState();
        //Debug.Log(playerString + " " + gamepad.IsConnected);
        CheckVessel();

        UpdateMode();
        if (mode == Mode.Ship || mode == Mode.Clone)
        {

            if (vessel != null)
            {
                UpdateVessel();
                //vessel.ToggleAI(false);
                UpdateMovementAxis();
                UpdateWeaponAxis();
                UpdateTriggerAxis();
                if (mode == Mode.Ship)
                {
                    UpdateFireMissile();
                    UpdateTeleport();
                }
                UpdateSelectionAxis();
            }
        }
        
        /*
        else if (ControllingShip())
        {
            UpdateSelectionAxis();
        }

        if (mode == Mode.Teleport)
        {
            UpdateMovementAxis();
        }*/
        
    }

    void UpdateVessel()
    {

        if (mode == Mode.Ship)
        {
            ship = vessel;
            clone = ship.CloneHub.GetFirst().GetComponent<VesselInput>();
        }
        if (mode == Mode.Clone)
        {
            clone = vessel;
            ship = clone.Ship;
        }
    }
    void GetGamePadState()
    {
        if (playerString == "P1") gamepad = GamePad.GetState(PlayerIndex.One);
        if (playerString == "P2") gamepad = GamePad.GetState(PlayerIndex.Two);
        if (playerString == "P3") gamepad = GamePad.GetState(PlayerIndex.Three);
        if (playerString == "P4") gamepad = GamePad.GetState(PlayerIndex.Four);
    }

    void CheckVessel()
    {
        if (vessel == null || vessel.gameObject.IsEnemyOf(gameObject))
        {
            NextVesselAfterDeath();
        }
    }

    bool ControllingShip()
    {
        return (mode == Mode.Ship || mode == Mode.SelectShip || mode == Mode.Teleport);
    }

    void UpdateMovementAxis()
    {
        vessel.movementAxis.x = gamepad.ThumbSticks.Left.X;
        vessel.movementAxis.y = gamepad.ThumbSticks.Left.Y;
    }

    void UpdateWeaponAxis()
    {
        vessel.weaponAxis.x = gamepad.ThumbSticks.Right.X;
        vessel.weaponAxis.y = gamepad.ThumbSticks.Right.Y;
        cursor = vessel.GetComponentInChildren<CursorControl>();
        if (cursor!=null) cursor.SetTarget(vessel.weaponAxis);
    }

    void UpdateFireMissile()
    {
        vessel.FireMissile = (gamepad.Buttons.B == ButtonState.Pressed);
    }

    void UpdateTriggerAxis()
    {
        vessel.triggerAxis = gamepad.Triggers.Right;
        vessel.shieldTrigger = gamepad.Triggers.Left;
    }

    void UpdateMode()
    {
        //UpdateSelectMode();
        UpdateVesselMode();
    }

    void UpdateSelectMode()
    {
        /*
        if (Input.GetButton(playerString + "LeftShoulder"))
        {
            if (mode == Mode.Ship)
            {
                mode = Mode.SelectShip;
                hub = shipHub;
                vessel.ToggleAI(true);
                if (cursor != null) cursor.Hide();
                if (cam != null) cam.ZoomFar(vessel.gameObject);
            }
            else if (mode == Mode.Clone)
            {
                mode = Mode.SelectClone;
                hub = ship.CloneHub;
                vessel.ToggleAI(true);
                if (cursor != null) cursor.Hide();
                if (cam != null) cam.ZoomCloseFar(ship.gameObject, clone.gameObject);
            }
        }
        else if (Input.GetButton(playerString + "RightShoulder"))
        {
            if (mode == Mode.Ship)
            {
                mode = Mode.Teleport;
                if (cursor != null) cursor.Hide();
            }
        }
        else
        {
            if (mode == Mode.SelectShip)
            {
                mode = Mode.Ship;

                vessel.ToggleAI(false);
                if (cursor != null) cursor.Show();
                if (cam != null) cam.ZoomMid(vessel.gameObject);
            }
            else if (mode == Mode.SelectClone)
            {
                mode = Mode.Clone;

                vessel.ToggleAI(false);
                if (cursor != null) cursor.Show();
                if (cam != null) cam.ZoomClose(ship.gameObject, clone.gameObject);
            }
            else if (mode == Mode.Teleport)
            {
                mode = Mode.Ship;
                if (cursor != null) cursor.Show();
            }
        }*/

    }

    void UpdateVesselMode()
    {
        if (ButtonADown())
        {
            if (mode == Mode.Clone && ship.CanBeControlBy(clone))
            {
                vessel = ship;
                mode = Mode.Ship;
                ship.FadeIn();
                if (cam != null) cam.ZoomMid(vessel.gameObject);
                vessel.SetTeam(clone.gameObject);
            }
            else if (mode == Mode.Ship)
            {
                vessel = clone;
                mode = Mode.Clone;
                ship.FadeOut();
                if (cam != null) cam.ZoomClose(ship.gameObject, clone.gameObject);
            }

        }
    }

    bool ButtonADown()
    {
        bool current = (gamepad.Buttons.A == ButtonState.Pressed);
        if (current && current != oldButtonA)
        {
            oldButtonA = current;
            return true;
        }
        else
        {
            oldButtonA = current;
            return false;
        }
    }

    bool ButtonXDown()
    {
        bool current = (gamepad.Buttons.X == ButtonState.Pressed);
        if (current && current != oldButtonX)
        {
            oldButtonX = current;
            return true;
        }
        else
        {
            oldButtonX = current;
            return false;
        }
    }



    void UpdateSelectionAxis()
    {
        float oldMagnitude = selectionAxis.magnitude;


        Vector2 tempAxis = new Vector2();

        if (gamepad.DPad.Left == ButtonState.Pressed)
        {
            tempAxis.x = -1;
        }
        if (gamepad.DPad.Right == ButtonState.Pressed)
        {
            tempAxis.x = 1;
        }
        if (gamepad.DPad.Up == ButtonState.Pressed)
        {
            tempAxis.y = 1;

        }
        if (gamepad.DPad.Down == ButtonState.Pressed)
        {
            tempAxis.y = -1;
        }

        selectionAxis = tempAxis.normalized;

        float newMagnitude = selectionAxis.magnitude;

        if (oldMagnitude < 0.5f && newMagnitude >= 0.5f)
        {
            SelectNextVessel();
        }
            //Debug.Log
            /*
            float oldMagnitude = selectionAxis.magnitude;
            if (mode == Mode.SelectShip || mode == Mode.SelectClone)
            {
                selectionAxis.x = Input.GetAxisRaw(playerString + "Horizontal");
                selectionAxis.y = Input.GetAxisRaw(playerString + "Vertical");
            }
            else if (mode == Mode.Teleport)
            {
                selectionAxis.x = Input.GetAxisRaw(playerString + "Horizontal2");
                selectionAxis.y = Input.GetAxisRaw(playerString + "Vertical2");
            }
            float newMagnitude = selectionAxis.magnitude;

            if (oldMagnitude < 0.5f && newMagnitude >= 0.5f)
            {
                if (mode == Mode.SelectShip || mode == Mode.SelectClone)
                {
                    SelectNextVessel();
                }
                else if (mode == Mode.Teleport)
                {
                    SelectTeleportTarget();
                }
            }*/
        }

    void SelectNextVessel()
    {
        GameObject next = shipHub.SelectNext(vessel.gameObject, selectionAxis);
        if (next != null)
        {
            vessel = next.GetComponent<VesselInput>();
            if (mode == Mode.Ship)
            {
                if (cam != null) cam.ZoomMid(vessel.gameObject);
                ship = vessel;
                clone = ship.CloneHub.GetFirst(tag).GetComponent<VesselInput>();
            }
            else if (mode == Mode.Clone)
            {
                if (cam != null) cam.ZoomCloseFar(ship.gameObject, vessel.gameObject);
                clone = vessel;
            }
        }
    }

    void NextVesselAfterDeath()
    {

        GameObject nextVessel = null;
        if (hub!= null) nextVessel = hub.GetFirst(tag);

        //No clones inside ship try to get next ship
        if (nextVessel == null && !ControllingShip())
        {
            nextVessel = shipHub.GetFirst(tag);
            mode = Mode.Ship;
        }

        //Vessel found
        if (nextVessel != null)
        {
            vessel = nextVessel.GetComponent<VesselInput>();
            if (ControllingShip())
            {
                cam.ZoomMid(vessel.gameObject);
                ship = vessel;
                mode = Mode.Ship;
                clone = ship.CloneHub.GetFirst(tag).GetComponent<VesselInput>();
            }
            else
            {
                clone = vessel;
                cam.ZoomClose(vessel.gameObject, ship.gameObject);
                mode = Mode.Clone;
            }
        }
        else
        {
            //GAME OVER
        }

    }

    void UpdateTeleport()
    {
        if (ButtonXDown())
        {
            SelectTeleportTarget();
        }
    }

    void SelectTeleportTarget()
    {

        GameObject target = shipHub.GetClosestEnemyShip(vessel.gameObject, vessel.tag);
        float dist = target.GetDistTo2D(vessel.gameObject);
        Debug.Log(dist);
        if (dist < 200)
        {
            GameObject teleportee = vessel.CloneHub.GetFirst(tag);
            ship = target.GetComponent<VesselInput>();
            Debug.Log("Teleportee" + teleportee);
            ship.Teleport(teleportee);

            vessel = teleportee.GetComponent<VesselInput>();
            vessel.DummyOffset = ship.DummyOffset;
            clone = vessel;
            hub = ship.CloneHub;
            mode = Mode.Clone;
            ship.FadeOut();
            if (cam != null) cam.ZoomClose(ship.gameObject, clone.gameObject);
        }
        

    }

    public VesselInput Vessel
    {
        get { return vessel; }
    }
}
