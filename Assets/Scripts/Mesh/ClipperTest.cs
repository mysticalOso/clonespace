﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ClipperLib;

namespace CloneSpace
{

    using Path = List<IntPoint>;
    using Paths = List<List<IntPoint>>;

    public class ClipperTest : MonoBehaviour
    {

        public GameObject cube;

        // Use this for initialization
        void Start()
        {
         
            Paths subj = new Paths(2);
            subj.Add(new Path(4));
            subj[0].Add(new IntPoint(180, 200));
            subj[0].Add(new IntPoint(260, 200));
            subj[0].Add(new IntPoint(260, 150));
            subj[0].Add(new IntPoint(180, 150));

            subj.Add(new Path(3));
            subj[1].Add(new IntPoint(215, 160));
            subj[1].Add(new IntPoint(230, 190));
            subj[1].Add(new IntPoint(200, 190));

            Paths clip = new Paths(1);
            clip.Add(new Path(4));
            clip[0].Add(new IntPoint(190, 210));
            clip[0].Add(new IntPoint(240, 210));
            clip[0].Add(new IntPoint(240, 130));
            clip[0].Add(new IntPoint(190, 130));

            DrawPolygons(subj);
            //DrawPolygons(clip);

            Paths solution = new Paths();

            Clipper c = new Clipper();
            c.AddPaths(subj, PolyType.ptSubject, true);
            c.AddPaths(clip, PolyType.ptClip, true);
            c.Execute(ClipType.ctIntersection, solution,
              PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);
            //DrawPolygons(solution);
        }

        // Update is called once per frame
        void Update()
        {

        }

        void DrawPolygons(Paths paths)
        {
            int size = 0;
            foreach (Path path in paths)
            {
                foreach (IntPoint point in path)
                {
                    size++;
                    
                }
            }
            Debug.Log(size);
            ;
            GameObject dummy = new GameObject();
            LineRenderer line = dummy.AddComponent<LineRenderer>();
            line.SetVertexCount(size);
            size = 0;
            foreach (Path path in paths)
            {
                foreach (IntPoint point in path)
                {
                    size++;
                    Debug.Log(point.X + "," + point.Y);
                    GameObject cube1 = Instantiate(cube);
                    Vector3 pos = new Vector3();
                    pos.x = point.X;
                    pos.z = point.Y;
                    pos.y = 0;
                    line.SetPosition(size - 1, pos);
                    cube1.transform.position = pos;
                }
            }
        }
    }
}
