﻿using UnityEngine;
using System.Collections.Generic;
using XInputDotNetPure;

public class DesignerShipMesh : MonoBehaviour 
{
    public Outline outline;
    ShipMeshGen shipMeshGen;
    public string playerString;
    public bool generated = false;
    GamePadState gamepad;

    void Start () 
	{
        shipMeshGen = new ShipMeshGen();
	}



    public void Generate()
    {
        if (!generated)
        {
            Mesh mesh = shipMeshGen.Generate(outline.GetOutline());
            GetComponent<MeshFilter>().mesh = mesh;
            ShipMeshStore.SaveMesh(mesh, playerString);
            ShipMeshStore.SaveOutline(outline.GetOutline(), playerString);

        }

        generated = true;
        //Application.LoadLevel("FourPlayer");
    }

    public bool IsReady
    {
        get {
            GetGamePadState();
            Debug.Log(playerString + " " + IsGamePadActive);
            return generated || !IsGamePadActive;
        }
    }

    bool IsGamePadActive
    {
        get
        {
            return gamepad.IsConnected;
        }
    }

    void GetGamePadState()
    {
        if (playerString == "P1") gamepad = GamePad.GetState(PlayerIndex.One);
        if (playerString == "P2") gamepad = GamePad.GetState(PlayerIndex.Two);
        if (playerString == "P3") gamepad = GamePad.GetState(PlayerIndex.Three);
        if (playerString == "P4") gamepad = GamePad.GetState(PlayerIndex.Four);
    }

}
