﻿using UnityEngine;
using System.Collections.Generic;

public class MeshModifier
{
    public MeshModifier()
    {

    }

    public void RoundMesh(Mesh mesh, List<Vector2> outline)
    {
        float maxDist = 0;
        Vector3[] verts = mesh.vertices;

        foreach (Vector3 v in verts)
        {
            Vector2 v1 = v;
            float dist = v1.DistToPoly(outline);
            if (dist > maxDist)
            {
                maxDist = dist;
            }
        }

        int i = 0;
        foreach (Vector3 v in verts)
        {
            Vector2 v1 = v;
            float dist = v1.DistToPoly(outline);
            float z = maxDist * Mathf.Sin(Mathf.Acos(0.9f - (dist / maxDist * 0.9f))) + 1;
            verts[i] = new Vector3(v.x, v.y, z);
            i++;
        }
        mesh.vertices = verts;
    }

    public void FlatShade(Mesh mesh)
    {
        Vector3[] oldVerts = mesh.vertices;
        int[] triangles = mesh.triangles;
        Vector3[] vertices = new Vector3[triangles.Length];
        for (int i = 0; i < triangles.Length; i++)
        {
            vertices[i] = oldVerts[triangles[i]];
            triangles[i] = i;
        }
        mesh.vertices = vertices;
        mesh.triangles = triangles;
    }

    public void AddNoise(Mesh mesh, float amount, float scale)
    {
        Vector3[] verts = mesh.vertices;
        NoiseGen noise = new NoiseGen();
        noise.Scale = scale;
        float half = amount * 0.5f;
        for (int i=0; i<verts.Length; i++)
        {
            Vector3 v = verts[i];
            v.z += noise.Get2D(v.x, v.y) * amount - half;
            verts[i] = v;
        }
        mesh.vertices = verts;
    }

    public void Optimise(Mesh mesh)
    {
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        ;
    }
}
