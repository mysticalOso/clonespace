﻿using UnityEngine;
using System.Collections;

public class ShipMesh : MonoBehaviour 
{

	void Start () 
	{
        string team = transform.parent.gameObject.GetTeamName();
        
        Mesh mesh = ShipMeshStore.LoadMesh(team);
        if (mesh == null)
        {
            ShipMeshGen meshGen = new ShipMeshGen();
            mesh = meshGen.Generate(FileInterface.LoadPoints("defaultShip.txt"));
        }
        GetComponent<MeshFilter>().mesh = mesh;
    }


}
