﻿using UnityEngine;
using System.Collections.Generic;

public class ShipMeshGen
{
    TriMeshGen meshGen;
    MeshModifier meshMod;


    public Mesh Generate(List<Vector2> outline1)
    {
        meshGen = new TriMeshGen();
        meshMod = new MeshModifier();
        List<Vector2> outline = RefineOutline(outline1);
        Mesh mesh = meshGen.GenerateMesh(outline, 0.1f);
        meshMod.RoundMesh(mesh,outline1);
        meshMod.FlatShade(mesh);
        meshMod.Optimise(mesh);
        return mesh;
    }


    List<Vector2> RefineOutline(List<Vector2> oldOutline)
    {
        List<Vector2> outline = new List<Vector2>();
        oldOutline.Reverse();
        int j = 0;
        int k = 0;
        foreach(Vector2 point in oldOutline)
        {
            k = j + 1;
            k = k % oldOutline.Count;
            float distance = Vector2.Distance(oldOutline[j], oldOutline[k]);
            int segs = (int)distance * 5;
            float div = 1.0f / (float)segs;
            for (int i=0; i<segs; i++)
            {
                float ratio = i * 0.1f;
                Vector2 newPoint = Vector2.Lerp(oldOutline[j], oldOutline[k], i * div);
                outline.Add(newPoint);
            }
            j++;
        }
        return outline;
    }


    void Update () 
	{
	
	}

}
