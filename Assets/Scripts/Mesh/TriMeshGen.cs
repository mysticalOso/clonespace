﻿using UnityEngine;
using System.Collections.Generic;
using TriangleNet.Geometry;
using TriangleNet.Data;
using System;

public class TriMeshGen
{
    TriangleNet.Mesh triMesh;
    InputGeometry geom;
    Mesh unityMesh;

    public Mesh GenerateMesh(List<Vector2> points, float triArea)
    {

        
        InitGeom(points);
        InitTriMesh(triArea);
        GenUnityMesh();

        return unityMesh;

    }

    

    private void GenUnityMesh()
    {
        unityMesh = new Mesh();

        Vector3[] verts = new Vector3[triMesh.Vertices.Count];
        Color[] colors = new Color[triMesh.Vertices.Count];
        int[] tris = new int[triMesh.Triangles.Count * 3];

        int i = 0;
        foreach (Vertex v in triMesh.Vertices)
        {
            Vector2 v1 = new Vector2((float)v.X, (float)v.Y);
            verts[i] = new Vector3(v1.x, v1.y, 0);
            i++;
        }

        i = 0;
        foreach (Triangle t in triMesh.Triangles)
        {
            tris[i] = t.P0;
            tris[i + 1] = t.P1;
            tris[i + 2] = t.P2;
            i += 3;
        }
        i = 0;
        foreach(int t in tris)
        {
            if (t >= verts.Length)
            {
                throw new Exception();
                //tris[i] = 0;
            }
            i++;
        }

        unityMesh.vertices = verts;
        unityMesh.triangles = tris;


    }


    private void InitTriMesh(float triArea)
    {
        triMesh = new TriangleNet.Mesh();
        triMesh.Behavior.Quality = true;
        triMesh.Behavior.MaxArea = triArea;
        triMesh.Triangulate(geom);
    }


    private void InitGeom(List<Vector2> points)
    {
        geom = new InputGeometry();
        int i = 0, j = 0;

        foreach (Vector2 point in points)
        {
            geom.AddPoint(point.x, point.y);
        }

        foreach (Vector2 point in points)
        {
            j = (i + 1) % points.Count;
            geom.AddSegment(i, j);
            i++;
        }

    }
}
