﻿using UnityEngine;
using System.Collections;

public class NoiseGen
{
    private float scale;
    private Vector2 offset2D;

    public NoiseGen(float scale1 = 1)
    {
        offset2D = Dice.Vector2(-10000000, -10000000, 10000000, 10000000);
        scale = scale1;
    }

    public float Get2D(float x, float y)
    {
        return Noise.Simplex2D(new Vector2(x, y) + offset2D, scale).value;
    }

    public float Get3D(float x, float y, float z, bool positive = false)
    {
        float noise = Noise.Simplex3D(new Vector3(x + offset2D.x, y + offset2D.y, z) , scale).value;
        if (positive)
        {
            noise = (noise + 1) * 0.5f;
        }
        return noise;
    }

    public float Scale
    {
        set
        {
            scale = value;
        }
    }

}
