﻿using UnityEngine;
using System.Collections;

public class InsideMeshExplosion : MonoBehaviour 
{
    ParticleSystem ps;
    ParticleSystem.Particle[] particles;
    Transform trans;
    Mesh mesh;

    void Start()
    {
        
        ps = GetComponent<ParticleSystem>();
        particles = new ParticleSystem.Particle[ps.maxParticles];
        ps.Emit(500);
        ps.GetParticles(particles);

        for (int i=0; i<particles.Length; i++)
        {
            int j = Random.Range(0, mesh.vertexCount);
            Vector3 pos = mesh.vertices[j];
            particles[i].position = pos;
            pos.z *= 0.5f;
            particles[i].velocity = pos * 5;
         
        }

        ps.SetParticles(particles, particles.Length);
    }

    public void LateUpdate()
    {
        

        
    }

    public GameObject MeshObject
    {
        set
        {
            trans = value.transform;
            mesh = value.GetComponent<MeshFilter>().mesh;
        }
    }


}
