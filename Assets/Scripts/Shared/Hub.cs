﻿using UnityEngine;
using System.Collections.Generic;

public class Hub : MonoBehaviour 
{
    public bool IsCloneHub = true;
    Hud hud;
    void Start()
    {
        if (IsCloneHub)
        {
            hud = transform.parent.GetComponentInChildren<Hud>();
        }
    }

    void Update()
    {
        if (IsCloneHub)
        {
            hud.SetCloneCount(FriendlyCloneCount());
        }
    }

    public int FriendlyCloneCount()
    {
        int count = 0;
        foreach (Transform child in transform)
        {
            if (child.gameObject.IsClone()) count++;
        }
        return count;
    }

    public GameObject GetFirst()
    {
        return transform.GetChild(0).gameObject;
    }

    public GameObject GetFirst(string tag)
    {
        GameObject first = null;
        foreach (Transform child in transform)
        {
            GameObject obj = child.gameObject;
            if (obj.tag == tag + "Ship" || obj.tag == tag + "Clone")
            {
                first = obj;
            }
        }
        return first;
    }

    public GameObject SelectNext(GameObject current, Vector2 direction)
    {
        if (transform.parent!= null)
        {
            direction = transform.parent.rotation * direction;
        }

        GameObject next = null;
        float minAngle = float.MaxValue;
        foreach (Transform child in transform)
        {
            GameObject target = child.gameObject;
            if (target.activeInHierarchy && target != current && target.CompareTag(current.tag))
            {
                Vector2 targetDirection = current.GetDirectionTo(target);
                float angle = Vector2.Angle(direction, targetDirection);
                if (angle < minAngle)
                {
                    next = target;
                    minAngle = angle;
                }
            }
        }
        return next;
    }

    public GameObject SelectNextEnemy(GameObject current, Vector2 direction)
    {
        if (transform.parent != null)
        {
            direction = transform.parent.rotation * direction;
        }

        GameObject next = null;
        float minAngle = float.MaxValue;
        foreach (Transform child in transform)
        {
            GameObject target = child.gameObject;
            if (target.activeInHierarchy && target != current && !target.CompareTag(current.tag))
            {
                Vector2 targetDirection = current.GetDirectionTo(target);
                float angle = Vector2.Angle(direction, targetDirection);
                if (angle < minAngle)
                {
                    next = target;
                    minAngle = angle;
                }
            }
        }
        return next;
    }

    public GameObject SelectEnemyClone(GameObject clone)
    {

        GameObject enemy = null;
        foreach (Transform child in transform)
        {
            GameObject target = child.gameObject;
            if (target.IsClone())
            {
                if (target.tag != clone.tag)
                {
                    enemy = target;
                }
            }

        }
        return enemy;
    }

    public bool ContainsEnemyClone(GameObject clone)
    {
        bool enemyFound = false;
        foreach (Transform child in transform)
        {
            GameObject target = child.gameObject;
            if (target.IsClone())
            {
                if (target.tag != clone.tag)
                {
                    enemyFound = true;
                }
            }

        }
        return enemyFound;
    }

    public GameObject GetClosestEnemyShip(GameObject subject, string tag)
    {
        GameObject enemy = null;
        float minDist = float.MaxValue;
        foreach (Transform child in transform)
        {
            GameObject target = child.gameObject;
            if (target.IsShip())
            {
                if (target.tag != tag)
                {
                    float dist = subject.GetDistTo2D(target);
                    if (dist < minDist)
                    {
                        enemy = target;
                        minDist = dist;
                    }
                    
                }
            }

        }
        return enemy;
    }

    public List<GameObject> GetAllShips()
    {
        List<GameObject> ships = new List<GameObject>();
        foreach (Transform child in transform)
        {
            GameObject target = child.gameObject;
            if (target.IsShip())
            {
                ships.Add(target);
            }
        }
        return ships;
    }

}
