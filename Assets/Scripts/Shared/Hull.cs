﻿using UnityEngine;
using System.Collections;

public class Hull : MonoBehaviour 
{

    public float health;
    float maxHealth;
    public GameObject explosion;
    public Hud hud;

    void Start()
    {
        maxHealth = health;
        hud = GetComponentInChildren<Hud>();
        if (hud != null) hud.SetHealthRatio(1);
    }

    //TODO Hit takes projectile type
    public virtual void Hit(float damage)
    {
        health-=damage;
        if (hud!= null)
        {
            hud.SetHealthRatio(health / maxHealth);
        }
        if (health <= 0)
        {
            CreateExplosion();
            Die();
        }
    }

    public virtual void Hit()
    {
        Hit(1);
    }


    public virtual void Die()
    {
        Destroy(gameObject);
    }

    public virtual void CreateExplosion()
    {
        GameObject e = Instantiate(explosion);
        e.transform.position = transform.position;
    }

}
