﻿using UnityEngine;
using System.Collections;

public class VesselInput : MonoBehaviour
{

    public Vector2 movementAxis;
    public Vector2 weaponAxis;
    public float triggerAxis;
    public bool fireMissile;
    public float shieldTrigger;
    AI ai;
    public CursorControl cursor;
    public Compass compass;
    int followers = 0;
    void Start()
    {
        compass = GetComponentInChildren<Compass>();
        cursor = GetComponentInChildren<CursorControl>();
        ai = GetComponent<AI>();
        //ToggleAI(ai.enabled);
    }

    public Vector2 Position
    {
        get
        {
            return gameObject.transform.position;
        }
    }

    //TODO cache values
    public Vector2 Direction
    {
        get
        {
            return movementAxis.normalized;
        }
    }

    public float Magnitude
    {
        get
        {
            return movementAxis.magnitude;
        }
    }

    public Vector2 WeaponDirection
    {
        get
        {
            return weaponAxis.normalized;
        }
    }

    public float WeaponMagnitude
    {
        get
        {
            return weaponAxis.magnitude;
        }
    }

    public bool IsFiring
    {
        get
        {
            return triggerAxis > 0;
        }
    }

    public bool FireMissile
    {
        get { return fireMissile; }
        set { fireMissile = value; }
    }

    public void TogglePlayerUI(bool isActive)
    {
        if(cursor!=null) cursor.gameObject.SetActive(isActive);
        if(compass!=null) compass.gameObject.SetActive(isActive);
        
    }

    public void FadeOut()
    {
        Fader fader = GetComponentInChildren<Fader>();
        if (fader != null) fader.FadeOut();
    }

    public void FadeIn()
    {
        Fader fader = GetComponentInChildren<Fader>();
        if (fader != null) fader.FadeIn();
    }

    //Separate below into ShipInput

    public Hub CloneHub
    {
        get
        {
            return GetComponentInChildren<Hub>();
        }
    }

    public void Teleport(GameObject clone)
    {
        Debug.Log("CloneHub = " + CloneHub);
        clone.transform.parent = CloneHub.transform;
    }

    public bool CanBeControlBy(VesselInput clone)
    {
        return !(CloneHub.ContainsEnemyClone(clone.gameObject));
    }

    public VesselInput Ship
    {
        get
        {
            return transform.parent.parent.GetComponent<VesselInput>();
        }
    }
  

    public Vector3 DummyOffset
    {
        get { return GetComponentInChildren<ShipInteriorGen>().DummyOffset; }
        
        //set should only available for clone
        set
        {
            GetComponent<DummyRigidbody>().Offset = value;
        }
    }

    public void SetTeam(GameObject clone)
    {
        if (clone.CompareTag("P1Clone"))
        {
            gameObject.tag = "P1Ship";
            GetComponentInChildren<ShipMaterial>().SetTeam("P1");
        }
        else if (clone.CompareTag("P2Clone"))
        {
            gameObject.tag = "P2Ship";
            GetComponentInChildren<ShipMaterial>().SetTeam("P2");
        }
        else if (clone.CompareTag("P3Clone"))
        {
            gameObject.tag = "P3Ship";
            GetComponentInChildren<ShipMaterial>().SetTeam("P3");
        }
        else if (clone.CompareTag("P4Clone"))
        {
            gameObject.tag = "P4Ship";
            GetComponentInChildren<ShipMaterial>().SetTeam("P4");
        }
    }

    public int Follow()
    {
        followers++;
        return followers;
    }

    public void UnFollow()
    {
        followers--;
    }

    public Quaternion Rotation
    {
        get { return gameObject.transform.rotation; }
    }
}

