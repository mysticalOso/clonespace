﻿using UnityEngine;
using System.Collections.Generic;

public class ShieldMeshGen : MonoBehaviour 
{
    TriMeshGen meshGen;
    MeshModifier meshMod;

    void Start () 
	{
        meshGen = new TriMeshGen();
        meshMod = new MeshModifier();
        ShapeMaker shape = new ShapeMaker();
        List<Vector2> circle = shape.MakeCircle(2, 24);
        Mesh mesh = meshGen.GenerateMesh(circle, 0.1f);
        meshMod.RoundMesh(mesh, circle);
        meshMod.FlatShade(mesh);
        meshMod.Optimise(mesh);
        GetComponent<MeshFilter>().mesh = mesh;

    }

}
