﻿using UnityEngine;
using System.Collections;

public class ShipEngine : MonoBehaviour {

    public float power;

    VesselInput input;
    ShipSteering steering;
    Rigidbody2D rb;
    public GameObject thrustPrefab;
    Thrust thrust;
	void Start () 
    {
        input = GetComponent<VesselInput>();
        steering = GetComponent<ShipSteering>();
        rb = GetComponent<Rigidbody2D>();
        thrust = Instantiate(thrustPrefab).GetComponent<Thrust>();
        thrust.target = rb;
	}
	
	void FixedUpdate () 
    {

        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        float angleTo = Vector2.Angle(transform.up, input.Direction);
        //Debug.Log(angleTo);
        //if (thrust!=null) thrust.SetPower(input.Magnitude);
        
    if (angleTo < 180)
    {
        float ratio = 1f - (angleTo / 180);
        if (thrust!=null) thrust.SetPower(input.Magnitude*ratio);
        rb.AddRelativeForce(Vector3.up * input.Magnitude * power* ratio);
    }
    else
    {
        if (thrust != null) thrust.SetPower(0);
    }
        //if (input.Magnitude == 0) thrust.LowerPower();
        //rb.AddRelativeForce(Vector3.up * input.Magnitude * power);
        rb.AddForce(input.Direction * power);

        
	}
}
