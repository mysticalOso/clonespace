﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class ShipInteriorGen : MonoBehaviour 
{
    public GameObject dummyWallPrefab;
    private Vector3 dummyOffset;
    public GameObject cloneHub;
    public GameObject clonePrefab;
    static int id = 0;

    void Awake()
    {
        string team = transform.parent.gameObject.GetTeamName();



        Mesh mesh = null;

        List<Vector2> points = ShipMeshStore.LoadOutline(team);
        if (points == null)
        {
            points = FileInterface.LoadPoints("defaultShip.txt");
        }
        
        //Vector2[] p = points.ToArray();
        //Array.Sort(p, new ClockwiseComparer(new Vector2(0,0)));
        //points = new List<Vector2>(p);

        TriMeshGen meshGen = new TriMeshGen();

        mesh = meshGen.GenerateMesh(points, 10000);
        mesh.RecalculateNormals();
        GetComponent<Renderer>().enabled = true;

        GetComponent<MeshFilter>().mesh = mesh;
        CreateClones();
        SetDummyOffset();
        CreateWalls(points.ToArray());
        SetCollider(points);

        
    }

    void CreateClones()
    {
        GameObject c1 = Instantiate(clonePrefab);
        c1.transform.parent = cloneHub.transform;
        c1.GetComponent<DummyRigidbody>().Position = new Vector3(-1, 0, 0);

        GameObject c2 = Instantiate(clonePrefab);
        c2.transform.parent = cloneHub.transform;
        c2.GetComponent<DummyRigidbody>().Position = new Vector3(1, 0, 0);
    }

    void CreateWalls(Vector2[] points)
    {
        GameObject dummy = Instantiate(dummyWallPrefab);
        dummy.transform.position = new Vector3(id * 1000f, id * 1000f, 0) + transform.localPosition;
        EdgeCollider2D walls = dummy.GetComponent<EdgeCollider2D>();
        Vector2[] newPoints = new Vector2[points.Length+1];
        
        for(int i=0; i<points.Length+1; i++)
        {
            int j = i % points.Length;
            //newPoints[i] = points[j] * 100f;
            newPoints[i] = points[j] ;
        }

        walls.points = newPoints;

    }

    void SetDummyOffset()
    {
        id++;
        DummyRigidbody[] dummies = gameObject.GetParent().GetComponentsInChildren<DummyRigidbody>();
        dummyOffset = new Vector3(id * 1000f, id * 1000f, 0);
        foreach ( DummyRigidbody d in dummies)
        {
            d.Offset = dummyOffset;
        }
    }

    public Vector3 DummyOffset
    {
        get { return dummyOffset; }
    }

    void SetCollider(List<Vector2> points)
    {
        PolygonCollider2D poly = transform.parent.GetComponent<PolygonCollider2D>();
        poly.SetPath(0, points.ToArray());
    }
}
