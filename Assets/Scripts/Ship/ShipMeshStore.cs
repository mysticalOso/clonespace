﻿using UnityEngine;
using System.Collections.Generic;

public static class ShipMeshStore
{
    static Dictionary<string, Mesh> meshes = new Dictionary<string, Mesh>();
    static Dictionary<string, List<Vector2>> outlines = new Dictionary<string, List<Vector2>>();

    public static void SaveMesh(Mesh mesh, string name)
    {
        meshes[name] = mesh;
    }

    public static void SaveOutline(List<Vector2> outline, string name)
    {
        outlines[name] = outline;
    }

    public static Mesh LoadMesh(string name)
    {
        if (meshes.ContainsKey(name))
        {
            return meshes[name];
        }
        else
        {
            return null;
        }
    }

    public static List<Vector2> LoadOutline(string name)
    {
        if (outlines.ContainsKey(name))
        {
            return outlines[name];
        }
        else
        {
            return null;
        }
    }

}
