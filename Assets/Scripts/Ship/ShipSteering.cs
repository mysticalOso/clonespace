﻿using UnityEngine;
using System.Collections;

public class ShipSteering : MonoBehaviour 
{
    VesselInput input;
    Vector2 targetDirection;
    Rigidbody2D rb;

    public float powerLimit = 50;
    public float powerMultiple = 1f;

	void Start () 
    {
        rb = GetComponent<Rigidbody2D>();
        input = GetComponent<VesselInput>();
	}
	
    public Vector2 TargetDirection
    {
        set
        {
            targetDirection = value;
        }
        get
        {
            return targetDirection;
        }
    }

   

	void FixedUpdate () 
    {
        TargetDirection = input.Direction;

        Vector2 currentDirection = new Vector2(transform.up.x, transform.up.y);
        
        if (input.Magnitude != 0)
        {

            float angleTo = Vector2.Angle(currentDirection, targetDirection);

            Vector3 cross = Vector3.Cross(currentDirection, targetDirection);

            if (cross.z > 0)
                angleTo = -angleTo;

            float torque = -angleTo * powerMultiple;


            torque = Mathf.Clamp(torque, -powerLimit, powerLimit);

            rb.AddTorque(torque);

        }
        
	}

    
}
