﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour 
{
    Vector3 nextPosition = new Vector3(10, 0, 0);
    float moveSpeed = 0.1f;

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, nextPosition, Time.deltaTime * moveSpeed);
    }


}
