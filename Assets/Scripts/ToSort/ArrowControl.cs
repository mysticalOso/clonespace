﻿using UnityEngine;
using System.Collections.Generic;

public class ArrowControl : MonoBehaviour 
{
    Vector2 direction;

	void Start () 
	{
	
	}

	void Update () 
	{
	
	}

    public void SetDirection(Vector2 direction)
    {
        transform.rotation = Quaternion.LookRotation(Vector3.forward, direction);
        transform.position = new Vector2(direction.x * 150f, direction.y * 150f) + (Vector2)transform.parent.position;
    }

    public void SetColour(Color c)
    {
        GetComponent<SpriteRenderer>().color = c;
    }

    public void SetTeam(string teamName)
    {
        if (teamName == "P1") SetColour(new Color(1, 1, 1, 0.4f));
        if (teamName == "P2") SetColour(new Color(1, 0.2f, 0.2f, 0.4f));
        if (teamName == "P3") SetColour(new Color(0, 1, 0, 0.4f));
        if (teamName == "P4") SetColour(new Color(0.8f, 0.8f, 0, 0.4f));
    }

}
