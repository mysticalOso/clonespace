﻿using UnityEngine;
using System.Collections.Generic;

public class Compass : MonoBehaviour 
{
    public GameObject arrowPrefab;
    List<ArrowControl> arrows;
    Hub shipHub;

	void Start () 
	{
        shipHub = GameObject.Find("Ships").GetComponent<Hub>();
        arrows = new List<ArrowControl>();
	}

	void LateUpdate () 
	{

        DestroyArrows();
        DrawArrows();

    }

    void DrawArrows()
    {
        List<GameObject> ships = shipHub.GetAllShips();

        foreach(GameObject ship in ships)
        {
            if (OutOfRange(ship))
            {
                GameObject arrowObj = Instantiate(arrowPrefab);
                arrowObj.transform.parent = transform;
                ArrowControl arrow = arrowObj.GetComponent<ArrowControl>();
                arrow.SetTeam(ship.GetTeamName());
                arrow.SetDirection(gameObject.GetDirectionTo(ship));
                arrows.Add(arrow);
            }
        }
    }

    void DestroyArrows()
    {
        foreach(ArrowControl arrow in arrows)
        {
            Destroy(arrow.gameObject);
        }
        arrows = new List<ArrowControl>();
    }

    bool OutOfRange(GameObject target)
    {

        Vector3 pos = target.transform.position;
        Vector3 centre = transform.position;
        float diffX = Mathf.Abs(pos.x - centre.x);
        if (diffX > 350) return true;
        float diffY = Mathf.Abs(pos.y - centre.y);
        if (diffY > 220) return true;

        return false;

    }

}
