﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class EndGameControl : MonoBehaviour 
{
    public Text winnerText;
    public Text gameoverText;
    public FadeAlphaChildren fader;
    public Hub shipHub;
    string team;

	public void GameOver()
    {
        team = gameObject.GetTeamName();
        winnerText.enabled = false;
        fader.FadeIn();
    }

    void Update()
    {
        bool allDead = true;
        List<GameObject> ships = shipHub.GetAllShips();
        foreach (GameObject ship in ships)
        {
            if (ship.IsFriendOf(gameObject))
            {
                allDead = false;
            }
        }

        if (allDead)
        {
            FadeToBlack();
        }
    }

    public void Winner()
    {
        gameoverText.enabled = false;
        fader.FadeIn();
    }

    public void FadeToBlack()
    {
        winnerText.enabled = false;
        gameoverText.enabled = false;
        fader.FadeIn();
    }

}
