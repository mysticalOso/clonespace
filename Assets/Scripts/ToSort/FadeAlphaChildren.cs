﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Line2D;
public class FadeAlphaChildren : MonoBehaviour 
{
    SpriteRenderer[] sprites;
    HexLine[] lines;
    Image[] images;
    Text[] texts;

    private float alpha;

    enum FadeState { FadingIn, FadingOut, FadedIn, FadedOut };

    FadeState state = FadeState.FadedOut;
    

    float fadeRatio = 0;

    public float fadeRate = 0.5f;


    void Start () 
	{

        UpdateAlpha();
    }


    void Update()
    {
        
        if (state == FadeState.FadingIn)
        {
            fadeRatio += fadeRate * Time.deltaTime;
            if (fadeRatio >= 1)
            {
                fadeRatio = 1;
                state = FadeState.FadedIn;
            }
            //UpdateAlpha();
        }

        if (state == FadeState.FadingOut)
        {
            fadeRatio -= fadeRate * Time.deltaTime;
            if (fadeRatio <= 0)
            {
                fadeRatio = 0;
                state = FadeState.FadedOut;
            }
            //UpdateAlpha();
        }
        UpdateAlpha();
    }

    void UpdateAlpha()
    {
        sprites = GetComponentsInChildren<SpriteRenderer>();
        lines = GetComponentsInChildren<HexLine>();
        images = GetComponentsInChildren<Image>();
        texts = GetComponentsInChildren<Text>();

        alpha = Mathf.Lerp(0, 1, fadeRatio);
        foreach(SpriteRenderer s in sprites)
        {
            Color c = s.color;
            c.a = alpha;
            s.color = c;
        }

        foreach (HexLine s in lines)
        {
            Color c = s.lineColor;
            c.a = alpha * 0.7f;
            s.lineColor = c;
        }

        foreach (Image s in images)
        {
            Color c = s.color;
            c.a = alpha;
            s.color = c;
        }

        foreach (Text s in texts)
        {
            Color c = s.color;
            c.a = alpha;
            s.color = c;
        }
    }

    public void FadeIn()
    {
        
        if (state != FadeState.FadedIn)
        {
            state = FadeState.FadingIn;
        }
    }

    public void FadeOut()
    {
       if (state != FadeState.FadedOut) state = FadeState.FadingOut;
    }

}
