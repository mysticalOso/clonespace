﻿using UnityEngine;
using System.Collections.Generic;

public class HeightBar : MonoBehaviour 
{
    Material mat;
	void Awake() 
	{
        mat = GetComponent<SpriteRenderer>().material;
	}

	public void SetRatio(float ratio)
    {
        mat.SetFloat("_MaskHeight", ratio);
    }

}
