﻿using UnityEngine;
using System.Collections.Generic;
using Line2D;
public class HexLine : MonoBehaviour 
{
    public GameObject hex1;
    public GameObject hex2;
    Line2DRenderer line;
    List<Line2DPoint> linePoints;
    public Color lineColor;
    public float width;
    PolygonCollider2D poly;
    public DistanceJoint2D joint1;
    public DistanceJoint2D joint2;
    float dist;
    int visibleCount = 2;
    public float zPos = 0;
    public Outline outline;

	void Start () 
	{
        linePoints = new List<Line2DPoint>();
        line = GetComponent<Line2DRenderer>();
        poly = GetComponent<PolygonCollider2D>();
        line.enabled = false;
	}

    public void SetHexes(GameObject h1, GameObject h2)
    {
        outline = h1.transform.parent.gameObject.GetComponent<Outline>();
        hex1 = h1;
        hex2 = h2;
        joint1.connectedBody = h1.GetComponent<Rigidbody2D>();
        joint2.connectedBody = h2.GetComponent<Rigidbody2D>();
        lineColor = hex1.GetComponent<MoveableHex>().Color;
        lineColor.a *= 0.7f;
    }

    void Update()
    {
        linePoints = new List<Line2DPoint>();
        Vector3 pos1 = hex1.transform.position;
        Vector3 pos2 = hex2.transform.position;
        pos1.z = zPos;
        pos2.z = zPos;
        linePoints.Add(new Line2DPoint(pos1, width, lineColor));
        linePoints.Add(new Line2DPoint(pos2, width, lineColor));
        line.points = linePoints;
        visibleCount--;
        if (visibleCount == 0) line.enabled = true;
    }

	void FixedUpdate () 
	{
        poly.points = CalculateCollider();
        UpdateJoints();
    }

    Vector2[] CalculateCollider()
    {
        Vector2[] p = new Vector2[4];
        dist = hex1.GetDistTo2D(hex2) * 0.5f;
        p[0] = new Vector2(-dist + 0.7f, -0.6f);
        p[1] = new Vector2(-dist + 0.7f, 0.6f);
        p[2] = new Vector2(dist - 0.7f, 0.6f);
        p[3] = new Vector2(dist - 0.7f, -0.6f);
        return p;
    }

    void UpdateJoints()
    {
        joint1.anchor = new Vector2(dist-0.7f, 0);
        joint2.anchor = new Vector2(-dist+0.7f, 0);
    }

    Vector2[] CalculateColliderOld()
    {
        Vector2[] p = new Vector2[4];
        Vector2 pos1 = hex1.transform.position;
        Vector2 pos2 = hex2.transform.position;
        Vector2 direction = (pos2 - pos1).normalized;
        Vector2 perp = new Vector2(-direction.y, direction.x) * 0.2f;

        p[0] = (pos1 + direction * 0.8f) + perp;
        p[1] = (pos2 - direction * 0.8f) + perp;
        p[2] = p[1] - perp * 2;
        p[3] = p[0] - perp * 2;

        return p;
    }
}
