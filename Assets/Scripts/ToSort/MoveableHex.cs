﻿using UnityEngine;
using System.Collections;

public class MoveableHex : MonoBehaviour 
{
    bool isMoving = false;
    GameObject mover;

	void Start () 
	{
	
	}

    public void Grab(GameObject grabber)
    {
        if (!isMoving)
        {
            isMoving = true;
            mover = grabber;
        }
    }

    public void Release(GameObject releaser)
    {
        if (mover == releaser)
        {
            isMoving = false;
            mover = null;
        }

    }

	void Update () 
	{
	    if (isMoving)
        {
            transform.position = mover.transform.position;
        }
	}

    public Color Color
    {
        get { return GetComponent<SpriteRenderer>().color; }
        set { GetComponent<SpriteRenderer>().color = value; }
    }
}
