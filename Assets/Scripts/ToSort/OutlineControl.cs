﻿using UnityEngine;
using System.Collections.Generic;

public class OutlineControl : MonoBehaviour 
{
    List<GameObject> hexes;
    List<Vector2> outline;
    Camera cam;
    LineRenderer line;
    void Start () 
	{
        hexes = new List<GameObject>();
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        line = GetComponent<LineRenderer>();
        outline = new List<Vector2>();
        foreach (Transform child in transform)
        {
            GameObject target = child.gameObject;
            hexes.Add(target);
        }
	}

	void Update () 
	{
        line.SetVertexCount(hexes.Count+1);
        outline.Clear();
        for (int i = 0; i <= hexes.Count; i++)
        {
            GameObject hex = hexes[i%hexes.Count];
            Vector3 pos = hex.transform.position;
            pos.z = 10;
            pos = cam.ScreenToWorldPoint(pos);
            line.SetPosition(i, pos);
            if (i!=hexes.Count) outline.Add(pos);
        }
	}

    public List<Vector2> Outline
    {
        get
        {
            return outline;
        }
    }

}
