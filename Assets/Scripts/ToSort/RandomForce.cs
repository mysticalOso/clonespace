﻿using UnityEngine;
using System.Collections;

public class RandomForce : MonoBehaviour {

    Rigidbody2D rb;
    public float power;
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        rb.AddForce(new Vector2(Random.Range(-power, power), Random.Range(-power, power)));
	}
}
