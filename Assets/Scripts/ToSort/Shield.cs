﻿using UnityEngine;
using System.Collections.Generic;

public class Shield : MonoBehaviour
{
    VesselInput input;
    MeshRenderer mesh;
    new CircleCollider2D collider;
    float energy = 100;
    int chargeDelay = 0;
    Hud hud;

    void Start()
    {
        input = transform.parent.GetComponentInChildren<VesselInput>();
        hud = transform.parent.GetComponentInChildren<Hud>();
        mesh = GetComponent<MeshRenderer>();
        collider = GetComponent<CircleCollider2D>();
    }

    public void Hit(float energyDamage)
    {
        Drain(energyDamage);
    }

    void Update()
    {
        if (IsOn)
        {
            mesh.enabled = true;
            Drain(0.25f);
        }
        else
        {
            mesh.enabled = false;
            Charge(0.15f);
        }

        hud.SetEnergy(energy / 100f);

        if (!CanBeOn) collider.isTrigger = true;
    }

    void Drain(float amount)
    {
        energy -= amount;
        if (energy <= 0)
        {
            energy = 0;
            chargeDelay = 200;
        }
    }

    void Charge(float amount)
    {
        if (chargeDelay == 0)
        {
            energy += amount;
            if (energy > 100) energy = 100;
        }
        else
        {
            chargeDelay--;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.IsBullet())
        {
            if (CanBeOn) collider.isTrigger = false;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (!other.gameObject.IsBullet())
        {
            collider.isTrigger = true;
        }
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        if (!coll.gameObject.IsBullet())
        {
            collider.isTrigger = true;
        }
    }

    public bool CanBeOn
    {
        get { return (energy > 0); }
    }

    public bool IsOn
    {
        get { return ((input.shieldTrigger > 0 || !collider.isTrigger) && CanBeOn); }
    }
}