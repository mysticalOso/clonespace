﻿using UnityEngine;
using System.Collections.Generic;

public class ShipMaterial : MonoBehaviour 
{
    public Material P1Mat;
    public Material P2Mat;
    public Material P3Mat;
    public Material P4Mat;

    Material current;
    new MeshRenderer renderer;

	void Start () 
	{
        renderer = GetComponent<MeshRenderer>();
        current = renderer.material;
	}

    public void SetTeam(string team)
    {
        if (team == "P1")
        {
            renderer.material = new Material(P1Mat);
        }
        if (team == "P2")
        {
            renderer.material = new Material(P2Mat);
        }
        if (team == "P3")
        {
            renderer.material = new Material(P3Mat);
        }
        if (team == "P4")
        {
            renderer.material = new Material(P4Mat);
        }
    }

}
