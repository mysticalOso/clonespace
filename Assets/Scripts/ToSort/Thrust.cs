﻿using UnityEngine;
using System.Collections.Generic;
using Line2D;

public class Thrust : MonoBehaviour {

    public Rigidbody2D target;
    public GameObject thrustParticlePrefab;
    List<GameObject> trail;
    public int maxTrailLength;
    public float power = 1;
    public float width = 3f;
    public Color startColor = new Color(1, 0.8f, 0, 1);
    public Color endColor = new Color(1, 0, 0, 0.2f);
    public Line2DRenderer lineRenderer;
    List<Line2DPoint> linePoints;
    public float powerTarget;
    public float jitter = 0.05f;
    public float propel = 2;
    bool dying = false;
    // Use this for initialization
    void Start ()
    {
        trail = new List<GameObject>();

	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (target == null) dying = true;
        if (!dying)
        {

        
        GameObject particle = Instantiate(thrustParticlePrefab);
        trail.Add(particle);
        particle.transform.position = target.transform.position;
        /*
        GameObject temp = new GameObject();

        temp.transform.rotation = 
        target.GetComponent<Rigidbody2D>().angularVelocity;*/
        Vector2 direction = (Vector2)target.gameObject.transform.up + Random.insideUnitCircle*0.05f;
        particle.GetComponent<Rigidbody2D>().velocity = -direction.normalized * target.velocity.magnitude * propel;
            //particle.transform.position += (Vector3)particle.GetComponent<Rigidbody2D>().velocity*0.01f;

        }


        while (trail.Count > maxTrailLength*power && trail.Count > 0)
        {
            GameObject p = trail[0];   
            trail.RemoveAt(0);
            Destroy(p);
        }

        linePoints = new List<Line2DPoint>();
        int i = 0;
        foreach(GameObject g in trail)
        {
            float ratio = (float)i*power / (float)trail.Count;
            Color c = Color.Lerp(endColor,startColor, ratio);
            ratio = (float)i / (float)trail.Count;
            float width1 = width * ratio;
            linePoints.Add(new Line2DPoint(g.transform.position,width1,c));
            i++;
        }

        lineRenderer.points = linePoints;

        if (power > powerTarget) LowerPower();
        if (power < powerTarget) AddPower();

        if (dying)
        {
            power -= 0.05f;
            if (power <= 0)
            {
                /*
                foreach (GameObject g in trail)
                {
                    Destroy(g);
                }
                */
                Destroy(gameObject);
            }

        }

	}

    public void AddPower()
    {
        power += 0.01f;
        if (power > 1) power = 1;
    }

    public void LowerPower()
    {
        power -= 0.01f;
        if (power < 0) power = 0;
    }

    public void SetPower(float p)
    {
        powerTarget = p;
    }

    public void Kill()
    {
        dying = true;
        powerTarget = 0;
    }
}
