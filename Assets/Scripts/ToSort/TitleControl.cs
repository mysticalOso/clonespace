﻿using UnityEngine;
using System.Collections.Generic;
using XInputDotNetPure;

public class TitleControl : MonoBehaviour
{
    public FadeAlphaChildren logo;
    public FadeAlphaChildren start;
    public FadeAlphaChildren designer;
    public GamePadState gamepad;
    int count = 0;

    public DesignerShipMesh ship1;
    public DesignerShipMesh ship2;
    public DesignerShipMesh ship3;
    public DesignerShipMesh ship4;


    enum State { Start, FadedIn, APressed };

    State state = State.Start;


    void Start()
    {

    }

    void FixedUpdate()
    {
        count++;
        if (state == State.Start)
        {
            if (count == 150)
            {
                logo.FadeIn();
            }
            if (count == 350)
            {
                start.FadeIn();
                state = State.FadedIn;
            }
        }

        if (state == State.APressed)
        {
            if (count == 250)
            {
                designer.gameObject.SetActive(true);
                designer.FadeIn();
            }
        }


    }

    void Update()
    {
        if (state == State.FadedIn || state == State.Start)
        {
            GetGamePadState("P1");
            if ((gamepad.Buttons.A == ButtonState.Pressed))
            {
                FadeOut();
            }
            GetGamePadState("P2");
            if ((gamepad.Buttons.A == ButtonState.Pressed))
            {
                FadeOut();
            }
            GetGamePadState("P3");
            if ((gamepad.Buttons.A == ButtonState.Pressed))
            {
                FadeOut();
            }
            GetGamePadState("P4");
            if ((gamepad.Buttons.A == ButtonState.Pressed))
            {
                FadeOut();
            }
        }
        if (state == State.APressed)
        {
            if (ship1.IsReady && ship2.IsReady && ship3.IsReady && ship4.IsReady)
            {
                Application.LoadLevel("FourPlayer");
            }
        }
    }

    void FadeOut()
    {
        state = State.APressed;
        logo.FadeOut();
        start.FadeOut();
        count = 0;
    }

    void GetGamePadState(string tag)
    {
        if (tag == "P1") gamepad = GamePad.GetState(PlayerIndex.One);
        if (tag == "P2") gamepad = GamePad.GetState(PlayerIndex.Two);
        if (tag == "P3") gamepad = GamePad.GetState(PlayerIndex.Three);
        if (tag == "P4") gamepad = GamePad.GetState(PlayerIndex.Four);
    }

}
