﻿using UnityEngine;
using System.Collections;

public class TorqueTest : MonoBehaviour 
{
    Rigidbody2D rb;
	void Start () 
	{
        rb = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate () 
	{
        rb.AddTorque(1);
	}

}
