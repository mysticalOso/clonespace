﻿using UnityEngine;
using System.Collections;

public class CursorControl : MonoBehaviour {

    RectTransform rect;
    Vector2 targetPosition;
    public CameraControl camera1;
    private Vector2 direction;
	// Use this for initialization
	void Start () 
    {
        rect = GetComponent<RectTransform>();
        direction = new Vector2(0, 1);
	}

    /*
GamePadState state = GamePad.GetState(PlayerIndex.One);
Console.WriteLine("IsConnected {0} Packet #{1}", state.IsConnected, state.PacketNumber);
Console.WriteLine("\tTriggers {0} {1}", state.Triggers.Left, state.Triggers.Right);
Console.WriteLine("\tD-Pad {0} {1} {2} {3}", state.DPad.Up, state.DPad.Right, state.DPad.Down, state.DPad.Left);
Console.WriteLine("\tButtons Start {0} Back {1} LeftStick {2} RightStick {3} LeftShoulder {4} RightShoulder {5} Guide {6} A {7} B {8} X {9} Y {10}",
state.Buttons.Start, state.Buttons.Back, state.Buttons.LeftStick, state.Buttons.RightStick, state.Buttons.LeftShoulder, state.Buttons.RightShoulder,
state.Buttons.Guide, state.Buttons.A, state.Buttons.B, state.Buttons.X, state.Buttons.Y);
Console.WriteLine("\tSticks Left {0} {1} Right {2} {3}", state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y, state.ThumbSticks.Right.X, state.ThumbSticks.Right.Y);
GamePad.SetVibration(PlayerIndex.One, state.Triggers.Left, state.Triggers.Right);

*/

	
	// Update is called once per frame
	void Update () 
    {
        targetPosition = new Vector2(direction.x * 70f, direction.y * 70f) + (Vector2)transform.parent.position;
        transform.position = Vector2.Lerp(transform.position, targetPosition, Time.deltaTime * 10f);
	}

    public void SetTarget(Vector2 direction)
    {
        if (direction.magnitude > 0.2f)
        {
            this.direction = direction.normalized;
            
        }
            
 

    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
}
