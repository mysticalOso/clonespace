﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class CursorInput : MonoBehaviour 
{

    Vector2 movementAxis = new Vector2();
    Rigidbody2D rb;
    GamePadState gamepad;
    public float power;
    public DesignerShipMesh meshGen;
    public bool oldButtonA;
    MoveableHex currentHex;


	void Start () 
	{
        rb = GetComponent<Rigidbody2D>();
	}

	void Update () 
	{
        GetGamePadState();
        UpdateMovementAxis();
        UpdateGrab();
        UpdateGenerate();
        currentHex = null;
	}

    void UpdateMovementAxis()
    {
        movementAxis.x = gamepad.ThumbSticks.Left.X;
        movementAxis.y = gamepad.ThumbSticks.Left.Y;
        if (movementAxis.magnitude > 0.1f) rb.AddForce(movementAxis * power);
    }

    void UpdateGrab()
    {
        if ((gamepad.Buttons.A == ButtonState.Pressed))
        {
            if (currentHex != null) currentHex.Grab(gameObject);
        }
        else
        {
            if (currentHex != null) currentHex.Release(gameObject);
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        MoveableHex hex = other.gameObject.GetComponent<MoveableHex>();

        if (hex!= null)
        {
            currentHex = hex;
        }
    }

    void GetGamePadState()
    {
        if (tag == "P1") gamepad = GamePad.GetState(PlayerIndex.One);
        if (tag == "P2") gamepad = GamePad.GetState(PlayerIndex.Two);
        if (tag == "P3") gamepad = GamePad.GetState(PlayerIndex.Three);
        if (tag == "P4") gamepad = GamePad.GetState(PlayerIndex.Four);
    }

    void UpdateGenerate()
    {
        if (Input.GetButtonDown(tag + "LeftShoulder"))
        {
            meshGen.Generate();
        }
    }

    bool ButtonADown()
    {
        bool current = (gamepad.Buttons.A == ButtonState.Pressed);
        if (current && current != oldButtonA)
        {
            oldButtonA = current;
            return true;
        }
        else
        {
            oldButtonA = current;
            return false;
        }
    }


}
