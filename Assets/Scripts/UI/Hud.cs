﻿using UnityEngine;
using System.Collections;

public class Hud : MonoBehaviour 
{
    public GameObject cloneIcons;
    public GameObject missileIcons;
    public HeightBar healthBar;
    public HeightBar energyBar;


    public void Update()
    {
        transform.localRotation = Quaternion.Inverse(transform.parent.localRotation);
    }

    public void SetCloneCount(int noClones)
    {
        int i = 0;
        foreach(Transform child in cloneIcons.transform)
        {
            GameObject cloneIcon = child.gameObject;
            i++;
            if (i <= noClones) cloneIcon.SetActive(true);
            else cloneIcon.SetActive(false);
        }
    }

    public void SetMissileCount(int noMissiles)
    {
        int i = 0;
        foreach (Transform child in missileIcons.transform)
        {
            GameObject missileIcon = child.gameObject;
            i++;
            if (i <= noMissiles) missileIcon.SetActive(true);
            else missileIcon.SetActive(false);
        }
    }

    public void SetHealthRatio(float ratio)
    {
        healthBar.SetRatio(ratio);
    }

    public void SetEnergy(float ratio)
    {
        energyBar.SetRatio(ratio);
    }

}
