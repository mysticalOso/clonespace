﻿using UnityEngine;
using System.Collections.Generic;
using Line2D;
public class Outline : MonoBehaviour 
{
    List<GameObject> hexes;
    public GameObject hexLinePrefab;
    public GameObject hexPrefab;
    public float newHexDist = 1;
    public Color color;
    public DesignerShipMesh mesh;

    void Start()
    {
        hexes = new List<GameObject>();

        foreach (Transform child in transform)
        {
            GameObject target = child.gameObject;
            if (target.activeSelf)
            {
                hexes.Add(target);
            }
            MoveableHex hex = target.GetComponent<MoveableHex>();
            if (hex != null) hex.Color = color;
        }

        for (int i = 0; i < hexes.Count; i++)
        {
            int j = (i + 1) % hexes.Count;
            AddHexline(hexes[i], hexes[j]);
        }

    }

    void Update()
    {
        foreach (Transform child in transform)
        {
            GameObject target = child.gameObject;
            MoveableHex hex = target.GetComponent<MoveableHex>();
            if (hex != null) hex.Color = color;
        }
    }
    void AddHexline(GameObject hex1, GameObject hex2)
    {
        HexLine hexLine = Instantiate(hexLinePrefab).GetComponent<HexLine>();
        hexLine.transform.parent = transform.parent;
        Vector3 pos = Vector3.Lerp(hex1.transform.position, hex2.transform.position, 0.5f);
        hexLine.transform.position = pos;
        hexLine.SetHexes(hex1, hex2);

    }

    public bool IsTooCloseToHex(GameObject subject)
    {
        bool result = false;
        for (int i = 0; i < hexes.Count; i++)
        {
            if (hexes[i].GetDistTo2D(subject) < newHexDist) result = true;
        }
        return result;
    }

    public bool AddNewHex(HexLine onLine, Vector2 pos)
    {
        if (onLine == null) return false;
        GameObject newHex = Instantiate(hexPrefab);
        newHex.transform.parent = transform;
        newHex.transform.position = pos;
        newHex.GetComponent<MoveableHex>().Color = color;
        GameObject hex1 = onLine.hex1;
        GameObject hex2 = onLine.hex2;
        int index = hexes.IndexOf(hex2);
        hexes.Insert(index, newHex);
        DestroyImmediate(onLine.gameObject);
        AddHexline(hex1, newHex);
        AddHexline(newHex, hex2); 
        return true;
    }

    public List<Vector2> GetOutline()
    {
        List<Vector2> outline = new List<Vector2>();
        foreach(GameObject o in hexes)
        {
            outline.Add(o.transform.localPosition);
        }
        return outline;
    }


}
