﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreControl : MonoBehaviour {

    public Text scoreText;
    int score = 0;

    public void AddScore()
    {
        score++;
        scoreText.text = "SCORE = " + score;
    }

	
}
