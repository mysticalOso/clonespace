﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class UIInput : MonoBehaviour 
{

    public Vector2 movementAxis = new Vector2();
    Rigidbody2D rb;
    GamePadState gamepad;
    public float power;
    public DesignerShipMesh meshGen;
    bool buttonBWasPressed = false;
    bool buttonAWasPressed = false;
    MoveableHex currentHex;
    DistanceJoint2D joint;
    public bool isMirror = false;
    bool isEnabled = true;
    public GameObject mirror;
    public GameObject hexSprite;
    bool canMakeNew = false;
    HexLine hexLine = null;

	void Start () 
	{
        rb = GetComponent<Rigidbody2D>();
        joint = GetComponent<DistanceJoint2D>();
        joint.enabled = false;
	}

	void Update () 
	{
        GetGamePadState();
        UpdateEnabled();
        if (isEnabled)
        {
            UpdateMovementAxis();
            UpdateGrab();
            UpdateGenerate();
            
        }
        currentHex = null;
        if (gamepad.Buttons.X == ButtonState.Pressed)
        {
            if (meshGen != null) meshGen.Generate();
        }
    }

    void UpdateEnabled()
    {
        if (ButtonBDown() && isMirror)
        {
            isEnabled = !isEnabled;
            if (!isEnabled && currentHex != null)
            {
                Release(currentHex.gameObject);
            }
            GetComponent<SpriteRenderer>().enabled = isEnabled;
        }
    }

    void UpdateMovementAxis()
    {
        movementAxis.x = gamepad.ThumbSticks.Left.X;
        movementAxis.y = gamepad.ThumbSticks.Left.Y;
        if (isMirror)
        {
            Vector2 mirrorPos = mirror.transform.localPosition;
            mirrorPos.x *= -1;
            transform.localPosition = mirrorPos;
        }
        else
        {
            if (movementAxis.magnitude > 0.2f) rb.AddForce(movementAxis * power);
        }
        
    }

    void UpdateGrab()
    {
        if ((gamepad.Buttons.A == ButtonState.Pressed))
        {
            if (currentHex != null) Grab(currentHex.gameObject);

        }
        else
        {
            if (currentHex != null) Release(currentHex.gameObject);
        }

        if (canMakeNew && ButtonADown()) MakeNew();
    }

    void MakeNew()
    {
        bool result = hexLine.outline.AddNewHex(hexLine, transform.position);
        if (!result) buttonAWasPressed = false;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        MoveableHex hex = other.gameObject.GetComponent<MoveableHex>();

        if (hex!= null)
        {
            currentHex = hex;
        }

        HexLine hexLine1 = other.gameObject.GetComponent<HexLine>();

        if (hexLine1 != null)
        {
            CheckHexLine(hexLine1);
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        HexLine hexLine1 = other.gameObject.GetComponent<HexLine>();

        if (hexLine1 != null)
        {
            hexSprite.SetActive(false);
            canMakeNew = false;
        }

    }

    void GetGamePadState()
    {
        if (tag == "P1") gamepad = GamePad.GetState(PlayerIndex.One);
        if (tag == "P2") gamepad = GamePad.GetState(PlayerIndex.Two);
        if (tag == "P3") gamepad = GamePad.GetState(PlayerIndex.Three);
        if (tag == "P4") gamepad = GamePad.GetState(PlayerIndex.Four);
    }

    void UpdateGenerate()
    {
        if (Input.GetButtonDown(tag + "LeftShoulder"))
        {
            //meshGen.Generate();
        }
    }

    bool ButtonBDown()
    {
        bool current = (gamepad.Buttons.B == ButtonState.Pressed);
        if (current && current != buttonBWasPressed)
        {
            buttonBWasPressed = current;
            return true;
        }
        else
        {
            buttonBWasPressed = current;
            return false;
        }
    }

    bool ButtonADown()
    {
        bool current = (gamepad.Buttons.A == ButtonState.Pressed);
        if (current && current != buttonAWasPressed)
        {
            buttonAWasPressed = current;
            return true;
        }
        else
        {
            buttonAWasPressed = current;
            return false;
        }
    }

    void Grab(GameObject g)
    {
        Rigidbody2D r = g.GetComponent<Rigidbody2D>();
        r.isKinematic = false;
        joint.connectedBody = r;
        joint.enabled = true;
        hexSprite.SetActive(false);
    }

    void Release(GameObject g)
    {
        if (joint.connectedBody != null)
        {
            Rigidbody2D r = g.GetComponent<Rigidbody2D>();
            r.isKinematic = true;
            joint.connectedBody = null;
            joint.enabled = false;
        }

    }


    void CheckHexLine(HexLine hexline1)
    {
        if (hexline1 != null)
        {
            if (!hexline1.outline.IsTooCloseToHex(gameObject))
            {
                hexSprite.SetActive(true);
                canMakeNew = true;
                hexLine = hexline1;
            }
            else
            {
                hexSprite.SetActive(false);
                canMakeNew = false;
            }
        }

    }
}
