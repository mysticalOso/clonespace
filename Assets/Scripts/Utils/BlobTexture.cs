﻿using UnityEngine;
using System.Collections;

public class BlobTexture : MonoBehaviour 
{
    public int width = 100;
    public int height = 100;
    private float blobX = 0;
    private Texture2D texture;
    BlobTextureGen blobGen;

    void Start () 
	{
        blobGen = new BlobTextureGen();
    }

    void Update()
    {
        blobX+=0.2f;
        blobGen.Zero();
        Texture2D texture = Texture2D.blackTexture;
        texture.Resize(width, height);
        //blobGen.Ones();
        blobGen.GenBlob(50, 50, 50);
        //blobGen.GenBlob(blobX, 50, 30);
        blobGen.ApplyNoise(0.01f, 0.5f);
        blobGen.ApplyNoise(0.02f, 0.5f);
        //blobGen.ApplyNoise(0.04f, 0.2f);
        //blobGen.ApplyNoise(0.06f, 0.1f);
        Color[] colors = texture.GetPixels();
        //Color[] colors = blobGen.GetColors(new Color(0.5f, 0.1f, 0f));

        for (int i = 0; i < colors.Length; i++)
        {
            float noise = blobGen.GetValue(i);
            if (blobGen.GetValue(i) > 0)
            {
                colors[i] = new Color(1.0f, 0.5f, 0f) * blobGen.GetValue(i);
            }
            else
            {
                colors[i] = new Color(0.1f, 0.5f, 0f) * -blobGen.GetValue(i);
            }
        }

        texture.filterMode = FilterMode.Point;
        texture.SetPixels(colors);
        texture.Apply(true);
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = texture;

        
    }

    public float[] Values
    {
        get
        {
            return blobGen.Values;
        }
    }

    


}
