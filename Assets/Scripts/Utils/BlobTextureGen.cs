﻿using UnityEngine;
using System.Collections;

public class BlobTextureGen
{
    private float[] values;
    private int width;
    private int height;
    private NoiseGen noise;
    private float z = 0;

    public BlobTextureGen(int size)
    {
        Init(size, size);
    }

    public BlobTextureGen(int width = 100, int height = 100)
    {
        Init(width, height);
    }

    private void Init(int width, int height)
    {
        values = new float[width * height];
        this.width = width;
        this.height = height;
        noise = new NoiseGen(0.02f);
        Zero();
    }

    public void GenBlob(float x1, float y1, float size)
    {
        float isize = 1.0f / size;
        float blob = 0;
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
            {
                int i = y * width + x;
                Vector2 diff = new Vector2(x - x1, y - y1);
                float dist = diff.magnitude * isize;
                if (dist <= 1) blob = (1 - dist);
                else blob = 0;
                if (values[i] < blob) values[i] = blob;
            }
    }

    public void Ones()
    {
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
            {
                int i = y * width + x;
                values[i] = 1;
            }
    }

    public void Zero()
    {
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
            {
                int i = y * width + x;
                values[i] = 0;
            }
    }

    public void ApplyNoise(float scale, float amount)
    {
        z+= 0.2f;
        noise.Scale = scale;
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
            {
                int i = y * width + x;
                float n = noise.Get3D(x, y, z, true) * amount;

                values[i] -= n;
                
                if (values[i] <= 0) values[i] = 0;
                //else values[i] = 1;
            }
    }

    public void GenNoise()
    {
        z+=2;
        for (int y = 0; y < height; y++)
            for (int x = 0; x <width; x++)
            {
                int i = y * width + x;
                float n = noise.Get3D(x, y, z);
                values[i] = n;
            }
    }

    public float GetValue(int i)
    {
        return values[i];
    }

    public Color[] GetColors(Color shade)
    {
        Color[] colors = new Color[width * height];

        for(int i = 0; i < colors.Length; i++)
        {
            colors[i] = new Color(shade.r,shade.g,shade.b);
            i++;
        }

        return colors;
    }

    public float[] Values
    {
        get
        {
            return values;
        }
    }


}
