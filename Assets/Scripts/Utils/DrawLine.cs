﻿using UnityEngine;
using System.Collections;

public class DrawLine : MonoBehaviour 
{
    LineRenderer line;
    BlobTexture blob;
    OutlineGen outline;

	void Start () 
	{
        line = GetComponent<LineRenderer>();
        blob = GetComponent<BlobTexture>();
        outline = new OutlineGen();
	}

	void Update () 
	{
        float[] values = blob.Values;
        Vector2[] points = outline.GenOutline(values, 100, 100);
        if (points.Length > 0)
        {
            line.SetVertexCount(points.Length + 1);
            int i = 0;
            foreach (Vector2 v in points)
            {
                line.SetPosition(i, new Vector3(v.x, v.y, -0.1f) * 0.01f);
                i++;
            }
            Vector2 v1 = points[0];
            line.SetPosition(i, new Vector3(v1.x, v1.y, -0.1f) * 0.01f);
        }

        gameObject.transform.position = -(Vector3)(outline.Shift) * 0.01f;



    }

}
