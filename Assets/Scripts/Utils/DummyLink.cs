﻿using UnityEngine;
using System.Collections;

public class DummyLink : MonoBehaviour
{
    GameObject link;
    public bool SendTriggerMsg = true;

    void Start()
    {

    }

    void Update()
    {

    }

    public GameObject Link
    {
        get { return link; }
        set { link = value; }
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (link != null && SendTriggerMsg) link.SendMessage("DummyTrigger", other);
 
    }
}
