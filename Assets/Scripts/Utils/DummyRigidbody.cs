﻿using UnityEngine;
using System.Collections;

public class DummyRigidbody : MonoBehaviour 
{
    public GameObject dummyPrefab;
    public float linearDrag;
    public GameObject dummy;
    Vector3 offset;
    Vector3 basePos;
	void Awake () 
	{
        dummy = Instantiate(dummyPrefab);
        DummyLink link = dummy.GetComponent<DummyLink>();
        if (link != null) link.Link = gameObject;
        dummy.transform.position = transform.localPosition;
        dummy.transform.rotation = transform.localRotation;
        Rb.drag = linearDrag;
	}

    void FixedUpdate()
    {
        Refresh();
    }

    public void Refresh()
    {
        transform.localPosition = Position;
        transform.localRotation = Rotation;
    }

    public void AddTorque(float torque)
    {
        Rb.AddTorque(torque);
    }

	public Rigidbody2D Rb
    {
        get { return dummy.GetComponent<Rigidbody2D>(); }
    }

    public Vector2 Velocity
    {
        set { Rb.velocity = value; }
        get { return Rb.velocity; }
    }

    public Vector3 Position
    {
        set {
            dummy.transform.position = value + Offset;
        }
        get { return dummy.transform.position - Offset;  }
    }

    public Quaternion Rotation
    {
        set { dummy.transform.rotation = value; }
        get { return dummy.transform.rotation; }
    }

    public Vector3 Offset
    {
        set {
            dummy.transform.position -= offset;
            offset = value;
            dummy.transform.position += offset;
        }
        get { return offset; }
    }

    public Vector2 CenterOfMass
    {
        get
        {
            return Rb.centerOfMass;
        }
        set
        {
            Rb.centerOfMass = value;
        }
    }


    public void OnDestroy()
    {
        Destroy(dummy);
    }
    

}
