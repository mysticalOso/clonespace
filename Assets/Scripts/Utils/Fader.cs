﻿using UnityEngine;
using System.Collections;

public class Fader : MonoBehaviour 
{
    private float alpha;

    enum FadeState { FadingIn, FadingOut, FadedIn, FadedOut };

    FadeState state = FadeState.FadedIn;

    Material material;

    float fadeRatio = 1;

    public float fadeRate = 0.5f;

	void Start () 
	{
        material = GetComponent<Renderer>().material;
	}

	void Update () 
	{
	    if (state == FadeState.FadingIn)
        {
            fadeRatio += fadeRate * Time.deltaTime;
            if (fadeRatio >= 1)
            {
                fadeRatio = 1;
                state = FadeState.FadedIn;
            }
            UpdateAlpha();
        }

        if (state == FadeState.FadingOut)
        {
            fadeRatio -= fadeRate * Time.deltaTime;
            if (fadeRatio <= 0)
            {
                fadeRatio = 0;
                state = FadeState.FadedOut;
                GetComponent<MeshRenderer>().enabled = false;
            }
            UpdateAlpha();
        }
    }

    void UpdateAlpha()
    {
        alpha = Mathf.Lerp(0, 1, fadeRatio);
        material.SetAlpha(alpha);
    }

    public void FadeIn()
    {
        /*
        if (state != FadeState.FadedIn)
        {
            GetComponent<MeshRenderer>().enabled = true;
            state = FadeState.FadingIn;
        }*/
    }

    public void FadeOut()
    {
        //if (state != FadeState.FadedOut) state = FadeState.FadingOut;
    }

}
