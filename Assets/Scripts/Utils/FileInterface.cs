﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class FileInterface
{

    public static void SavePoints(List<Vector2> points, string fileName)
    {
        StreamWriter sr = File.CreateText(fileName);

        sr.WriteLine(points.Count);

        foreach(Vector2 point in points)
        {
            sr.WriteLine(point.x);
            sr.WriteLine(point.y);
        }
        sr.Close();
    }

    public static List<Vector2> LoadPoints(string fileName)
    {
        List<Vector2> points = new List<Vector2>();

        if (File.Exists(fileName))
        {
            StreamReader sr = File.OpenText(fileName);
            int count = int.Parse(sr.ReadLine());

            for (int i=0; i<count; i++)
            {
                float x = float.Parse(sr.ReadLine());
                float y = float.Parse(sr.ReadLine());
                points.Add(new Vector2(x, y));
            }

            
        }
        else {
            Debug.Log("Could not Open the file: " + fileName + " for reading.");
            return null;
        }


        return points;
    }


}
