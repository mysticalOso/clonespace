﻿using UnityEngine;
using System.Collections.Generic;

public class InversePos : MonoBehaviour 
{

	void LateUpdate () 
	{
        transform.localPosition = -transform.parent.localPosition;
	}

}
