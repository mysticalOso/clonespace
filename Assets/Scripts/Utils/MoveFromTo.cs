﻿using UnityEngine;
using System.Collections.Generic;

public class MoveFromTo : MonoBehaviour 
{

    public Vector3 startPos;
    public Vector3 endPos;

    public void SetMoveRatio(float ratio)
    {
        transform.position = Vector3.Lerp(startPos, endPos, ratio);
    }

}
