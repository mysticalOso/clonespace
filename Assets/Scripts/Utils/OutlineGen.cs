﻿using UnityEngine;
using System.Collections.Generic;

public class OutlineGen
{
    private int width = 100;
    private int height = 100;
    private float[] values;
    private List<Vector2> points;
    private List<Vector2> poly;
    private Vector2 shift;

    struct Point
    {
        public int X, Y;
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
    public OutlineGen()
    {

    }

    public Vector2[] GenOutline(float[] values1, int width, int height)
    {
        points = new List<Vector2>();

        values = values1;
        

        Point p1 = new Point();

        int i = 0;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                i = x + y * width;
                if (values[i] > 0)
                {
                    p1.X = x;
                    p1.Y = y;
                    x = width;
                    y = height;
                }
            }
        }
        DrawEdge(p1.X, p1.Y);

        poly = new List<Vector2>();
        i = 0;
        shift = new Vector2();
        Vector2 centre = new Vector2(width * 0.5f, height * 0.5f);
        foreach (Vector2 p in points)
        {
            i++;

                shift += (p - centre);
            
        }

        shift /= points.Count;

            i = 0;
        foreach (Vector2 p in points)
        {
            i++;
            if (i % 4 == 0)
            {
                //poly.Add(new Vector2(p.x - width * 0.5f, p.y - height * 0.5f));
                poly.Add(new Vector2(p.x, p.y) - shift - centre);
            }

        }
        

        return poly.ToArray();
    }

    void DrawEdge(int startX, int startY)
    {
        int c = 0;

        int x = startX;
        int y = startY;
        int i;
        int lastEdge = -5;

        do
        {
            c++;
            i = x + y * width;
            points.Add(new Vector2(x, y));
            for (int a = 0; a < 8; a++)
            {
                int j = (a + lastEdge + 5) % 8;

                if (TestEdge(x, y, j))
                {
                    lastEdge = j;
                    Point p = IntToEdge(j);
                    x += p.X;
                    y += p.Y;
                    a = 8;
                }
            }

        } while ((!(startX == x && startY == y) && c < 500));
        if (c >= 500) points = new List<Vector2>();
    }

    bool TestEdge(int x, int y, int edge)
    {
        Point p = IntToEdge(edge);
        x += p.X;
        y += p.Y;
        int i = x + y * width;
        if (i >= 0 && i < width * height)
        {
            if (values[i] > 0)
            {
                values[i] = 0;
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    Point IntToEdge(int i)
    {
        if (i == 0) return new Point(-1, 0);
        else if (i == 1) return new Point(-1, 1);
        else if (i == 2) return new Point(0, 1);
        else if (i == 3) return new Point(1, 1);
        else if (i == 4) return new Point(1, 0);
        else if (i == 5) return new Point(1, -1);
        else if (i == 6) return new Point(0, -1);
        else if (i == 7) return new Point(-1, -1);
        else return new Point(0, 0);
    }

    public Vector2 Shift
    {
        get
        {
            return shift;
        }
    }

    public List<Vector2> Poly
    {
        get { return poly; }
    }
}
