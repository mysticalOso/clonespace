﻿using UnityEngine;
using System.Collections.Generic;

public class ShapeMaker
{

    public List<Vector2> MakeCircle(float radius, int noPoints)
    {
        List<Vector2> points = new List<Vector2>();

        for (int i = 0; i < noPoints; i++)
        {
            float ratio = (float)i / (float)noPoints;
            float y = Mathf.Cos(ratio * 2 * Mathf.PI) * radius;
            float x = Mathf.Sin(ratio * 2 * Mathf.PI) * radius;
            points.Add(new Vector2(x, y));
        }

        return points;
    }
}
