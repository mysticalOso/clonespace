﻿using UnityEngine;
using System.Collections.Generic;

public class SpawnPrefabs : MonoBehaviour 
{
    public GameObject prefab;
    public int count;
    public Vector2 minPos;
    public Vector2 maxPos;
    public float zPos;
    public float minDistance;
    float minSqrDistance;
    List<Vector2> points;

    void Start () 
	{
        minSqrDistance = minDistance * minDistance;
        points = new List<Vector2>();
	    for (int i=0; i< count; i++)
        {
            GameObject obj = Instantiate(prefab);
            Vector3 pos = new Vector3();

            do
            {
                pos = Dice.Vector2(minPos, maxPos);
            } while (TooClose(pos));

            points.Add(pos);
            pos.z = zPos;
            obj.transform.position = pos;
        }
	}


    bool TooClose(Vector2 pos)
    {
        bool isTooClose = false;
        foreach(Vector2 p in points)
        {
            float sqrDist = Vector2.SqrMagnitude(pos - p);
            if (sqrDist < minSqrDistance) isTooClose = true;
        }
        return isTooClose;
    }

}
