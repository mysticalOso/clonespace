﻿using UnityEngine;
using System.Collections.Generic;

public class Test : MonoBehaviour 
{
    Rigidbody2D rb;
    public float XForce;
	void Start () 
	{
        rb = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate () 
	{
        rb.AddForce(new Vector2(XForce, 0));
	}

}
