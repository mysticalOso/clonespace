﻿using UnityEngine;
using System.Collections;

public class CloneLazerBeam : LazerBeam 
{
    private DummyRigidbody dummy;
    
    public override void Fire(LazerCannon cannon)
    {
        dummy = GetComponent<DummyRigidbody>();

        


        transform.parent = cannon.transform.parent;

        dummy.Offset = cannon.DummyOffset;
        

        dummy.Position = cannon.Position + (Vector3)cannon.FireVelocity * 0.01f;
        dummy.Rotation = cannon.Rotation;
        dummy.Velocity = cannon.FireVelocity;


        dummy.Refresh();




    }

    public void DummyTrigger(Collider2D other)
    {
        GameObject hitObject = other.gameObject;
        if (hitObject.CompareTag("Wall"))
        {
            GameObject e = Instantiate(explosion);
            e.transform.parent = transform.parent;
            e.transform.localPosition = transform.localPosition;
            Destroy(gameObject);
        }

        DummyLink link = hitObject.GetComponent<DummyLink>();

        if (link != null)
        {
            hitObject = link.Link;

            if (hitObject.IsClone())
            {
                if (hitObject.tag != owner)
                {
                    GameObject e = Instantiate(explosion);
                    e.transform.parent = transform.parent;
                    e.transform.localPosition = transform.localPosition;
                    Destroy(gameObject);
                    hitObject.GetComponent<Hull>().Hit();
                }
            }
        }

    }
}
