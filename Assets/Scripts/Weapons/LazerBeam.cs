﻿using UnityEngine;
using System.Collections;

public class LazerBeam : MonoBehaviour {

    int life = 200;
    protected string owner = "";

    public GameObject explosion;
    public GameObject shieldExplosion;

	void Start () 
    {
	
	}

    public virtual void Fire(LazerCannon cannon)
    {
        transform.localRotation = cannon.Rotation;
        transform.localPosition = cannon.Position;

        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        rb.velocity = cannon.FireVelocity;

        rb.transform.localPosition += (Vector3)rb.velocity * 0.01f;
    }
	
	public virtual void FixedUpdate () 
    {
        life--;
        if (life == 0) Destroy(gameObject);
	}

    //TODO improving ship tagging/ownership
    void OnTriggerEnter2D(Collider2D other)
    {
        GameObject hitObject = other.gameObject;
        if (hitObject != null)
        {
            if (hitObject.IsShip() || hitObject.IsAsteroid())
            {
                if (hitObject.tag != owner)
                {
                    GameObject e = Instantiate(explosion);
                    e.transform.position = transform.position;
                    Destroy(gameObject);
                    hitObject.GetComponent<Hull>().Hit();
                }
            }
            if (hitObject.IsShield())
            {
                if (hitObject.GetTeamName() != owner.Substring(0, 2))
                {
                    Shield shield = hitObject.GetComponent<Shield>();
                    if (shield.IsOn)
                    {
                        GameObject e = Instantiate(shieldExplosion);
                        e.transform.position = transform.position;
                        Destroy(gameObject);
                        shield.Hit(10);
                    }

                }
            }


        }
    }

    public void SetOwner(string name)
    {
        owner = name;
    }

}
