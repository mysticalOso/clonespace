﻿using UnityEngine;
using System.Collections;

public class LazerCannon : MonoBehaviour 
{

    VesselInput input;
    Rigidbody2D rb;
    public float rate = 10;
    public float power = 200;
    public GameObject lazerPrefab;
    public bool cloneWeapon = false;
    int count = -1;

    Shield shield;
    Hud hud;

    Vector2 targetDirection = new Vector2(0,1);

	void Start () 
    {
        input = GetComponent<VesselInput>();
        if (!cloneWeapon) rb = GetComponent<Rigidbody2D>();
        hud = GetComponentInChildren<Hud>();
        shield = GetComponentInChildren<Shield>();
        //StartCoroutine(Fire());
    }

    public Vector2 TargetDirection
    {
        set
        {
            targetDirection = value;
        }
        get
        {
            return Quaternion.Inverse(transform.parent.rotation) * targetDirection;
        }
    }

    public Vector3 Position
    {
        get
        {
            return transform.localPosition;
        }
    }

    public Quaternion Rotation
    {
        get
        {
            return Quaternion.LookRotation(Vector3.forward, TargetDirection);
        }
    }

    public Vector2 FireVelocity
    {
        get
        {
            Vector2 fireVelocity = new Vector2(TargetDirection.x, TargetDirection.y) * power;
            if (!cloneWeapon) fireVelocity += rb.velocity;
            return fireVelocity;
        }
    }

    public Vector3 DummyOffset
    {
        get
        {
            if (cloneWeapon)
            {
                return GetComponent<DummyRigidbody>().Offset;
            }
            else
            {
                return new Vector3();
            }
        }
    }

    // Update is called once per frame
    void Update () 
    {

        if (input.WeaponMagnitude > 0)
        {
            if (cloneWeapon)
            {
                TargetDirection = input.gameObject.transform.up;
            }
            else
            {
                TargetDirection = input.WeaponDirection;
            }
            
        }
	}
    
    void FixedUpdate()
    {
        if (input.IsFiring && count == -1) count = 0;
  
        if (input.IsFiring && count==0)
        {
            if (cloneWeapon || !shield.IsOn)
            {
                count = 0;
                GameObject lazer = Instantiate(lazerPrefab);
                lazer.GetComponent<LazerBeam>().Fire(this);
                lazer.SendMessage("SetOwner", gameObject.tag);
            }

        }

        if (count >= 0)
        {
            count++;
        }

        if (count == rate)
        {
            count = -1;

        }
    }
    /*
    IEnumerator Fire() 
    {
        
        while(true)
        {
            if (input.IsFiring)
            {
                yield return new WaitForFixedUpdate();
                GameObject lazer = Instantiate(lazerPrefab);
                lazer.transform.rotation = Quaternion.LookRotation(Vector3.forward, TargetDirection);
                lazer.transform.position = transform.position;
                Rigidbody lazerRb = lazer.GetComponent<Rigidbody>();

                lazerRb.velocity = new Vector3(TargetDirection.x, TargetDirection.y, 0) * 200f + rb.velocity;
                Debug.Log("Lazer Velocity = " + lazerRb.velocity.normalized.ToString("F4"));
                Debug.Log("velocity2 = " + rb.velocity.ToString("F4"));
                lazer.transform.position += lazerRb.velocity * 0.01f;
                
            }
            yield return new WaitForSeconds(rate);
        }

    }*/

}
