﻿using UnityEngine;
using System.Collections.Generic;

public class Missile : MonoBehaviour 
{
    protected int life = 200;
    protected string owner = "";
    protected Rigidbody2D rb;
    protected Hub shipHub;
    protected GameObject target;
    protected Vector2 targetDirection;
    public float turningPower;
    public float thrustPower;

    public GameObject thrustPrefab;
    public Thrust thrust;
    public GameObject explosion;
    public GameObject shieldExplosion;

    protected virtual void Start () 
	{
        rb = GetComponent<Rigidbody2D>();
        
    }

    public void Fire(MissileLauncher launcher)
    {
        transform.localRotation = launcher.Rotation;
        transform.localPosition = launcher.Position;
        

        rb = GetComponent<Rigidbody2D>();
        GameObject ships = GameObject.Find("Ships");
        shipHub = ships.GetComponent<Hub>();

        rb.velocity = launcher.FireVelocity + (Vector2)transform.up * 200f;
        rb.transform.localPosition += (Vector3)rb.velocity * 0.01f;
        owner = launcher.Owner;


        thrust = Instantiate(thrustPrefab).GetComponent<Thrust>();
        thrust.target = rb;

        thrust.powerTarget = 1;
    }

	protected virtual void FixedUpdate () 
	{
        FindTarget();
        GetTargetDirection();
        AdjustDirectionForSpeed();
        TurnTowardsTarget();
        ThrustForward();

        life--;
        if (life == 0) Explode();

	}

    void ThrustForward()
    {
        //rb.AddRelativeForce(Vector2.up * thrustPower*0.1f);
        rb.AddForce(targetDirection * thrustPower);
    }

    void TurnTowardsTarget()
    {

        
        Vector2 currentDirection = new Vector2(transform.up.x, transform.up.y);
        
        float angleTo = Vector2.Angle(currentDirection, targetDirection);

        Vector3 cross = Vector3.Cross(currentDirection, targetDirection);

        if (cross.z > 0)
            angleTo = -angleTo;

        float torque = -(angleTo) * turningPower;
        
        rb.AddTorque(torque);
        
        
    }

    void GetTargetDirection()
    {
        targetDirection = gameObject.GetNextVector2To(target).normalized;
    }

    void AdjustDirectionForSpeed()
    {
        Vector2 relativeVelocity = gameObject.GetVelocityRelativeTo2D(target);
        float relativeSpeed = relativeVelocity.magnitude;

        float lazerSpeed = 400f;

        float angleFromVelocityToTarget = relativeVelocity.AngleTo(targetDirection);

        float angleOffset = Mathf.Asin((relativeSpeed * Mathf.Sin(angleFromVelocityToTarget)) / lazerSpeed);
        if (targetDirection.magnitude > 0)
        {
            targetDirection = targetDirection.RotateVector(-angleOffset).normalized;
        }
    }

    void FindTarget()
    {
        target = shipHub.GetClosestEnemyShip(gameObject, owner);
    }

    void Explode()
    {
        GameObject e = Instantiate(explosion);
        e.transform.position = transform.position;
        thrust.Kill();
        Destroy(gameObject);
        
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        GameObject hitObject = other.gameObject;
        if (hitObject != null)
        {
            if (hitObject.IsShip() || hitObject.IsAsteroid())
            {
                if (hitObject.tag != owner)
                {
                    
                    hitObject.GetComponent<Hull>().Hit(5);
                    Explode();
                }
            }
            if (hitObject.IsShield())
            {
                if (hitObject.GetTeamName() != owner.Substring(0, 2))
                {
                    Shield shield = hitObject.GetComponent<Shield>();
                    if (shield.IsOn)
                    {
                        GameObject e = Instantiate(shieldExplosion);
                        e.transform.position = transform.position;
                        e = Instantiate(explosion);
                        e.transform.position = transform.position;
                        Destroy(gameObject);
                        shield.Hit(50);
                    }

                }
            }

        }
    }
}
