﻿using UnityEngine;
using System.Collections.Generic;

//TODO renameTolauncher
public class MissileLauncher : MonoBehaviour 
{
    VesselInput input;
    Rigidbody2D rb;
    public GameObject missilePrefab;
    Shield shield;
    Hud hud;
    public int rate = 100;
    int count = 0;
    int ammo = 4;

	void Start () 
	{
        input = GetComponent<VesselInput>();
        rb = GetComponent<Rigidbody2D>();
        hud = GetComponentInChildren<Hud>();
        shield = GetComponentInChildren<Shield>();
        hud.SetMissileCount(ammo);
    }

    public void Update()
    {
        if (input.FireMissile && CanFire())
        {
            FireMissile();
            count = rate;
        }
    }



    void FireMissile()
    {
        GameObject missile = Instantiate(missilePrefab);
        missile.GetComponent<Missile>().Fire(this);
        ammo--;
        hud.SetMissileCount(ammo);
    }

    public Vector3 Position
    {
        get
        {
            return transform.localPosition;
        }
    }

    public string Owner
    {
        get
        {
            return gameObject.tag;
        }
    }

    public Quaternion Rotation
    {
        get
        {
            return transform.rotation;
        }
    }

    public Vector2 FireVelocity
    {
        get
        {
            return rb.velocity;
        }
    }


    void FixedUpdate () 
	{
	    if (count > 0)
        {
            count--;
        }
	}

    bool CanFire()
    {
        return (count <= 0 && !shield.IsOn && ammo > 0);
    }

}
