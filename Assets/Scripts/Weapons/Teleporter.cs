﻿using UnityEngine;
using System.Collections.Generic;

public class Teleporter : Missile
{

    protected override void Start()
    {



        rb = GetComponent<Rigidbody2D>();
        GameObject ships = GameObject.Find("Ships");
        //shipHub = ships.GetComponent<Hub>();

        rb.velocity = (Vector2)transform.up * 200f;
        rb.transform.localPosition += (Vector3)rb.velocity * 0.01f;


        thrust = Instantiate(thrustPrefab).GetComponent<Thrust>();
        thrust.target = rb;

        thrust.powerTarget = 1;
    }

    protected override void FixedUpdate()
    {
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {

    }

}
